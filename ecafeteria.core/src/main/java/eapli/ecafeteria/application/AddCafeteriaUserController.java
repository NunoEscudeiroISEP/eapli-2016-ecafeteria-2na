/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.application;

import static eapli.ecafeteria.AppSettings.ensurePermissionOfLoggedInUser;
import eapli.ecafeteria.domain.authz.ActionRight;
import eapli.ecafeteria.domain.authz.RoleType;
import eapli.ecafeteria.domain.authz.SystemUser;
import eapli.ecafeteria.domain.authz.UserBuilder;
import eapli.ecafeteria.domain.cafeteria.OrganicUnit;
import eapli.ecafeteria.domain.cafeteria.OrganicUnitBuilder;
import eapli.ecafeteria.domain.mealbooking.Account;
import eapli.ecafeteria.domain.mealbooking.CafeteriaUser;
import eapli.ecafeteria.domain.mealbooking.CafeteriaUserBuilder;
import eapli.ecafeteria.domain.mealbooking.MecanographicNumber;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.persistence.AccountRepository;
import eapli.ecafeteria.persistence.CafeteriaUserRepository;
import eapli.ecafeteria.persistence.OrganicUnitRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.ecafeteria.persistence.UserRepository;
import eapli.framework.application.Controller;
import eapli.framework.persistence.DataIntegrityViolationException;
import eapli.util.DateTime;
import java.util.Calendar;
import java.util.Set;

/**
 *
 * @author vitoralexandremascarenhasmascarenhas
 */
public class AddCafeteriaUserController implements Controller{

   public CafeteriaUser addCafeteriaUser(String username, String password, String firstName, String lastName, String email,
            Set<RoleType> roles, Calendar createdOn, String mecanographicNumber, String acronym, String name, String description, String account){
        //ensurePermissionOfLoggedInUser(ActionRight.CafeteriaUser);
    
        try{
        // create a cafeteria user builder
        final CafeteriaUserBuilder cafeteriaUserBuilder = new CafeteriaUserBuilder();
        
        
        // create a user builder
        final UserBuilder userBuilder = new UserBuilder();
        // add a atributes at userbuilder

        userBuilder.withUsername(username).withPassword(password).withFirstName(firstName).withLastName(lastName)
                .withEmail(email).withCreatedOn(createdOn).withRoles(roles);
        
        // create a object systemuser
        SystemUser systemUser = userBuilder.build();
        
        // add a system user at cafeteria builder
        //cafeteriaUserBuilder.withSystemUser(systemUser);
        
        // create a instance of organicUnit builder
        
        
        // add a organic unit at cafeteria user
        
        
        // create a mecanographic number
        final MecanographicNumber newMecanographicNumber = new MecanographicNumber(mecanographicNumber);
        // add mecanographic at cafetaria user builder
        cafeteriaUserBuilder.withMecanographicNumber(mecanographicNumber);
        
        // Create a account
        final Account newAccount = new Account(account, newMecanographicNumber);
        // add account at cafeteria user builder
        cafeteriaUserBuilder.withAccount(newAccount);
        CafeteriaUser newCafeteriaUser = cafeteriaUserBuilder.build();
        
        final OrganicUnitRepository organicUnitRepository = PersistenceContext.repositories().organicUnits();
        OrganicUnit org = organicUnitRepository.all().iterator().next();
        cafeteriaUserBuilder.withOrganicUnit(org);
        
                final UserRepository userRepository = PersistenceContext.repositories().users();
                userRepository.add(systemUser);
                SystemUser s = userRepository.findById(systemUser.username());
                System.out.println("Teste User "+ s.username().toString());
            
                cafeteriaUserBuilder.withSystemUser(s);
        //organicUnitRepository.add(organicUnit);
                
        //final AccountRepository accountsRepository = PersistenceContext.repositories().accounts();
        //accountsRepository.add(newAccount);
        
        
        //userRepository.add(systemUser);
        
        final CafeteriaUserRepository cafetariaUserRepository = PersistenceContext.repositories().cafeteriaUsers();
        cafetariaUserRepository.add(newCafeteriaUser);
        
        CafeteriaUser asd = cafetariaUserRepository.getCafetariaUser(systemUser);
        
            System.out.println("asdasdasd " + asd.mecanographicNumber().toString());
        
        return newCafeteriaUser;
        }catch(Exception e){
            System.out.println("addCafeterController " + e.toString());
            return null;
        }
    }

}