/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.application;

import eapli.ecafeteria.AppSettings;
import eapli.ecafeteria.domain.authz.Password;
import eapli.ecafeteria.domain.authz.SystemUser;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.ecafeteria.persistence.UserRepository;
import eapli.framework.application.Controller;

/**
 *
 * @author guest
 */
public class ChangePasswordController implements Controller{
    
    public boolean changePassword(Password password){
        SystemUser sysUser=AppSettings.instance().session().authenticatedUser();
        if(!sysUser.changePassword(password)){
            return false;
        }
        final UserRepository userRepository = PersistenceContext.repositories().users();
        // TODO error checking if the username is already in the persistence
        // store
        userRepository.save(sysUser);
        return true;
    }
    
    public boolean samePassword(Password password){
        SystemUser sysUser=AppSettings.instance().session().authenticatedUser();
        if(sysUser.passwordMatches(password)){
            return true;
        }
        return false;
    }
}
