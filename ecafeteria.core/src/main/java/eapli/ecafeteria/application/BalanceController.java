/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.application;

import static eapli.ecafeteria.AppSettings.ensurePermissionOfLoggedInUser;
import eapli.ecafeteria.domain.authz.ActionRight;
import eapli.ecafeteria.domain.mealbooking.Account;
import eapli.ecafeteria.domain.mealbooking.MecanographicNumber;
import eapli.ecafeteria.persistence.MovementsRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.ecafeteria.persistence.jpa.JpaMovementsRepository;
import eapli.framework.persistence.DataIntegrityViolationException;

/**
 *
 * @author Vitor Mascarenhas
 */
public class BalanceController {

    public BalanceController(){
    }
    
    public double getBalance(Account account) throws DataIntegrityViolationException{
        return account.getBalance();
    }
    
}