/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.application;

import eapli.ecafeteria.domain.meals.DishType;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.domain.meals.Menu;
import eapli.ecafeteria.domain.register.Register;
import eapli.ecafeteria.persistence.DishTypeRepository;
import eapli.ecafeteria.persistence.MealRepository;
import eapli.ecafeteria.persistence.MenuRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.ecafeteria.persistence.RepositoryFactory;
import eapli.framework.application.Controller;
import eapli.util.DateTime;
import java.util.List;

/**
 *
 * @author André Oliveira
 */
public class RegistrationOfNotSoldMealsController implements Controller {

    private Menu selectedMenu;
    
    public Boolean activeRegister(){
        if(Register.getInstance().isOpen()){
            return true;
        }
        
        return false;
    }
    
    public Boolean findMenuByDate() {
        RepositoryFactory repositoryFactory = PersistenceContext.repositories();

        MenuRepository menuRepository = repositoryFactory.menus();

        this.selectedMenu = menuRepository.findByPeriod();

        if (this.selectedMenu != null) {
            return true;
        } else {
            return false;
        }

    }

    public Iterable<DishType> listAllMealsTypes() {

        RepositoryFactory repositoryFactory = PersistenceContext.repositories();
        DishTypeRepository dishesType = repositoryFactory.dishTypes();

        Iterable<DishType> dishesTypeList = dishesType.all();

        return dishesTypeList;

    }
    
    public List<Meal> mealsOfType(DishType dishType){
        return this.selectedMenu.mealOfType(dishType);
    }
    
    public Meal registrationOfNotSoldMeals(Meal meal){
        meal.numberOfNotSoldMeals();
        
        RepositoryFactory repositoryFactory = PersistenceContext.repositories();
        MealRepository mealRepository = repositoryFactory.meals();
        
        return mealRepository.updateMeal(meal);
    }

}
