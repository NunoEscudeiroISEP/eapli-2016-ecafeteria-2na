/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.application;

import static eapli.ecafeteria.AppSettings.ensurePermissionOfLoggedInUser;
import eapli.ecafeteria.domain.authz.ActionRight;
import eapli.ecafeteria.domain.register.Register;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.ecafeteria.persistence.RegisterRepository;
import eapli.framework.application.Controller;
import eapli.framework.persistence.DataIntegrityViolationException;

/**
 *
 * @author guest
 */
public class AddRegisterController implements Controller {
    public Register addRegister(String ref, String desc, String name, String orgU)
            throws DataIntegrityViolationException {
        ensurePermissionOfLoggedInUser(ActionRight.Administer);

        final Register newRegister = new Register(ref, desc, name, orgU);
        final RegisterRepository registerRepository = PersistenceContext.repositories().registers();
        // TODO error checking if the acronym is already in the persistence
        // store
        registerRepository.add(newRegister);
        return newRegister;
    }
}