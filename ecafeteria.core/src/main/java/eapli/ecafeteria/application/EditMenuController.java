/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.application;

import eapli.ecafeteria.domain.meals.*;
import eapli.ecafeteria.persistence.DishRepository;
import eapli.ecafeteria.persistence.DishTypeRepository;
import eapli.ecafeteria.persistence.MenuRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.framework.application.Controller;
import java.util.ArrayList;

/**
 *
 * @author soliveira
 */
public class EditMenuController implements Controller {

    private DishRepository dishRepo;
    private final MenuRepository menuRepo;
    private Menu selectedMenu;

    public EditMenuController() {
        menuRepo = PersistenceContext.repositories().menus();
    }

    public Iterable<Menu> allMenus() {
        return menuRepo.all();
    }

    public void setMenuToEdit(Menu menu) {
        this.selectedMenu = menu;
    }

    public Menu defineNewPeriod(int weekNumber) {
        Period newPeriod = new Period(weekNumber);

        selectedMenu.defineNewPeriod(newPeriod);
        return menuRepo.save(selectedMenu);
    }

    public Iterable<Meal> allMealsForSelectedMenu() {
        if(this.selectedMenu != null){
            return this.selectedMenu.meals();
        }
        else{
            return new ArrayList<>();
        }
    }
  
    public void deleteMeal(Meal meal){
        menuRepo.deleteMeal(selectedMenu, meal);
    }
    
    public void editNumberOfDishes(Meal meal, int newNumberOfDishes){
        meal.changeNumberOfDishes(newNumberOfDishes);
        
        menuRepo.updateMeal(selectedMenu, meal);
    }

    public void editMealType(MealType mealType, Meal meal) {
        meal.changeMealType(mealType);
        
        menuRepo.updateMeal(selectedMenu, meal);
    }
}
