/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.application;

import eapli.ecafeteria.AppSettings;
import static eapli.ecafeteria.AppSettings.ensurePermissionOfLoggedInUser;
import eapli.ecafeteria.domain.authz.ActionRight;
import eapli.ecafeteria.domain.authz.Session;
import eapli.ecafeteria.domain.authz.SystemUser;
import eapli.ecafeteria.domain.mealbooking.Account;
import eapli.ecafeteria.domain.mealbooking.Booking;
import eapli.ecafeteria.domain.mealbooking.MecanographicNumber;
import eapli.ecafeteria.domain.mealbooking.Movement;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.domain.meals.Menu;
import eapli.ecafeteria.persistence.AccountRepository;
import eapli.ecafeteria.persistence.BookingsRepository;
import eapli.ecafeteria.persistence.CafeteriaUserRepository;
import eapli.ecafeteria.persistence.MenuRepository;
import eapli.ecafeteria.persistence.MovementsRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.framework.application.Controller;
import eapli.framework.persistence.DataIntegrityViolationException;

import java.util.Calendar;
import java.sql.Date;
import java.util.Iterator;


/**
 *
 * @author Sara Ramos
 */
public class RegisterBookingController implements Controller
{
    Menu menu;
 
    public Menu showCurrentMenu() {
            
       
        final MenuRepository repo = PersistenceContext.repositories().menus();
        this.menu = repo.findByPeriod();
        
        return this.menu;
    }
    
    public Meal registerBooking(Meal meal)throws DataIntegrityViolationException {
        ensurePermissionOfLoggedInUser(ActionRight.CafeteriaUser);
    
        SystemUser sysUser;
        Booking booking;
        MecanographicNumber mecNum;
        Session session = AppSettings.instance().session(); 
        
        sysUser = session.authenticatedUser();
        CafeteriaUserRepository repoUser = PersistenceContext.repositories().cafeteriaUsers(); //alterar mome da classe cafetaria repository no diagrama
        
        mecNum = repoUser.getCafetariaUser(sysUser).id();
        
        AccountRepository repoAccount = PersistenceContext.repositories().accounts();
        
        Iterable<Account> listAccount = repoAccount.findAccountByMecanographicNumber(mecNum);

        Iterator<Account> accounts = listAccount.iterator();
        Account userAccount = accounts.next();

        //TODO Check if user can have multiple accounts
        if(userAccount == null || accounts.hasNext()){
            throw new IllegalArgumentException("Error duplicated account in database");
        }
                
        float mealPrice = meal.price();
    
        double value = userAccount.getBalance();
        if(mealPrice <= value){ 
            //verifica se o utilizador pode fazer a reserva tendo em conta os limites de tempo para marcação
            Date bookingDate = new Date(Calendar.getInstance().getTimeInMillis());
            if(meal.mealType().checkTime(meal, bookingDate) && meal.isAvailable()){
                                               
                //Atualiza o saldo do utilizador
                Movement movement = new Movement(userAccount, bookingDate, "booking", mealPrice*-1);
                               
                MovementsRepository repoMovement = PersistenceContext.repositories().movements();
                repoMovement.save(movement);
                
                booking = new Booking(meal, bookingDate , mecNum); //alterar diagrama - não incliu preço
                BookingsRepository repoBooking = PersistenceContext.repositories().bookings();
                repoBooking.save(booking);
                
                value -=mealPrice;
                userAccount.updateBalance(value);
                
                //atualiza o número de refeições
                meal.decrementAvailableDishes();
                                
                
            }else{
                System.out.println("It is no longer possible to register your booking! Time limit passed.\n ");
            }
           
        }else{
            System.out.println("Current balance insufficient\n ");
        }              
        
        return meal;
    }
  
}
