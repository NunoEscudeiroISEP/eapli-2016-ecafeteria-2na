/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.application;

import eapli.ecafeteria.domain.mealbooking.criteria.BookingCriteriaFactory;
import eapli.ecafeteria.domain.mealbooking.criteria.ICriteria;
import eapli.ecafeteria.domain.mealbooking.criteria.ICriteriaFactory;
import eapli.ecafeteria.domain.mealbooking.Booking;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.ecafeteria.persistence.RepositoryFactory;
import eapli.ecafeteria.persistence.BookingsRepository;
import eapli.framework.application.Controller;

import java.util.List;

/**
 *
 * @author Pedro Costa
 */
public class ListExistingBookingsController implements Controller
{
    public List<Booking> listExistingBookings(String criteriaName, String criteriaValue)
    {
        //Create criteria
        ICriteriaFactory factory = new BookingCriteriaFactory();
        ICriteria criteria = factory.generateBookingCriteria(criteriaName, criteriaValue);
        
        RepositoryFactory repositoryFactory = PersistenceContext.repositories();
        BookingsRepository bookingsRepository = repositoryFactory.bookings();
        
        List<Booking> reservationsList = bookingsRepository.findByCriteria(criteria);
        
        return reservationsList;
    }
    
    public Iterable<Booking> listAllBookings()
    {
        RepositoryFactory repositoryFactory = PersistenceContext.repositories();
        BookingsRepository bookingsRepository = repositoryFactory.bookings();

        return bookingsRepository.all();
    }

    
}
