/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.application;

import eapli.ecafeteria.domain.cafeteria.OrganicUnit;
import eapli.ecafeteria.domain.register.Register;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.ecafeteria.persistence.RegisterRepository;
import eapli.framework.application.Controller;

/**
 *
 * @author guest
 */
public class OpenRegisterController implements Controller {
    
    private OrganicUnit organicUnit;
    private Register register;
    
    public OpenRegisterController() {
        this.register=PersistenceContext.repositories().registers().all().iterator().next();
        
    }
    
    public boolean isOpen(){
        return this.register.isOpen();
    }
    
    public boolean openRegister()
    {
        return this.register.setOpenRegister();
        
    }
    
    public boolean saveRegister(){
        final RegisterRepository registerRepository =PersistenceContext.repositories().registers();
        registerRepository.save(this.register);
        return true;
    }
    
}
