/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.application;

import eapli.ecafeteria.domain.meals.*;
import eapli.ecafeteria.persistence.DishRepository;
import eapli.ecafeteria.persistence.DishTypeRepository;
import eapli.ecafeteria.persistence.MenuRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.framework.application.Controller;
import eapli.framework.persistence.DataIntegrityViolationException;

/**
 *
 * @author soliveira
 */
public class GenerateMenuController implements Controller {

    private DishTypeRepository dishTypeRepo;
    private DishRepository dishRepo;

    private final MenuBuilder menuBuilder;
    private MealBuilder mealBuilder;

    public GenerateMenuController() {
        menuBuilder = new MenuBuilder();
    }

    public void generatePeriod(int weekNumber) {
        menuBuilder.referringToWeek(weekNumber);
    }

    /**
     * 2 -
     *
     * @param weekDay o dia da semana
     */
    public void defineWeekDay(int weekDay) {

        mealBuilder = new MealBuilder(menuBuilder.getPeriod()); // É preciso verificar se o period n é null
        mealBuilder.forWeekDay(weekDay);
    }

    public void defineDish(Dish dish) {
        mealBuilder.withDish(dish);
    }

    public void defineMealType(MealType mealType) {
        mealBuilder.forMealType(mealType);
    }

    public void defineNumberOfDishes(int numberOfDishes) {
        mealBuilder.withNumberOfDishes(numberOfDishes);
        
        menuBuilder.withMeal(mealBuilder.build());
    }

    public Iterable<DishType> listDishTypes() {
        dishTypeRepo = PersistenceContext.repositories().dishTypes();

        return dishTypeRepo.activeDishTypes();
    }

    public Iterable<Dish> AllDishes(DishType selectedDishType) {
        dishRepo = PersistenceContext.repositories().dishes();

        return dishRepo.allOfType(selectedDishType);
    }
    
    public void save() throws DataIntegrityViolationException{
        MenuRepository menuRepo = PersistenceContext.repositories().menus();
        
        menuRepo.save(menuBuilder.build());
    }

}
