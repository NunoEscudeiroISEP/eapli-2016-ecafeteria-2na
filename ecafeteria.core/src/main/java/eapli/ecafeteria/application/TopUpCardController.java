/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.application;

import eapli.ecafeteria.domain.cafeteria.OrganicUnit;
import eapli.ecafeteria.domain.mealbooking.Account;
import eapli.ecafeteria.domain.mealbooking.MecanographicNumber;
import eapli.ecafeteria.domain.register.Register;
import eapli.ecafeteria.persistence.AccountRepository;
import eapli.ecafeteria.persistence.DishTypeRepository;
import eapli.framework.application.Controller;
import javax.persistence.*;

/**
 *
 * @author 1111407
 */
public class TopUpCardController implements Controller {
    
    private OrganicUnit organicUnit;
    private Register register;
    
    public TopUpCardController() {
        this.register = new Register();
    }
    
    public boolean TopUpCard(String mecanograpic_number_string) throws IllegalStateException
    {
        return true;
    }
    
    public boolean verifyAccount(String mecanograpic_number_string) throws IllegalStateException
    {           
        MecanographicNumber mecanograpic_number = new MecanographicNumber(mecanograpic_number_string);
        
        // TODO - Ver como verificar se existe a conta
        EntityManagerFactory factory = Persistence.createEntityManagerFactory("Account");
        EntityManager manager = factory.createEntityManager();

        Query query = manager.createQuery("select * from Account where mecanographicNumber = " + mecanograpic_number_string);
        
        return !query.getResultList().isEmpty();          
    }
    
    public void createNewAccount(String mecanograpic_number_string) {        
        EntityManagerFactory factory = Persistence.createEntityManagerFactory("account");
        EntityManager manager = factory.createEntityManager();
        
                  
        MecanographicNumber mecanograpic_number = new MecanographicNumber(mecanograpic_number_string);
        
        Account account = new Account(mecanograpic_number_string, mecanograpic_number);
        
        final AccountRepository accountsRepository = eapli.ecafeteria.persistence.PersistenceContext.repositories().accounts();
        accountsRepository.save(account);
        
        manager.getTransaction().begin();
        manager.persist(account);
        manager.getTransaction().commit();        
    }
}
