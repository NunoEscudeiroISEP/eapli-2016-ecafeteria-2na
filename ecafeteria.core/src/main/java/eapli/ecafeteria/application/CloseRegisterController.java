/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.application;

import eapli.ecafeteria.domain.cafeteria.OrganicUnit;
import eapli.ecafeteria.domain.register.Register;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.ecafeteria.persistence.RegisterRepository;
import eapli.framework.application.Controller;

/**
 *
 * @author guest
 */
public class CloseRegisterController implements Controller {
    
    private OrganicUnit organicUnit;
    private Register register;
    
    public CloseRegisterController() {
        this.register=PersistenceContext.repositories().registers().all().iterator().next();
    }
    
    public boolean isClose(){
        return !this.register.isOpen();
    }
    
    public boolean closeRegister()
    {
        this.register.setCloseRegister();
        return true;
    }

    public boolean saveRegister(){
        final RegisterRepository registerRepository =PersistenceContext.repositories().registers();
        registerRepository.save(this.register);
        return true;
    }    
}
