package eapli.ecafeteria.application;

import eapli.ecafeteria.domain.authz.ActionRight;
import eapli.ecafeteria.domain.meals.Dish;
import eapli.ecafeteria.domain.meals.DishType;
import eapli.ecafeteria.domain.meals.NutritionalInfo;
import eapli.ecafeteria.persistence.DishRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.framework.application.Controller;
import eapli.framework.persistence.DataIntegrityViolationException;

import static eapli.ecafeteria.AppSettings.ensurePermissionOfLoggedInUser;

/**
 * Created by berme on 4/22/2016.
 */
public class RegisterDishController implements Controller{
    
    public Iterable<DishType> listActiveDishTypes() {
        return new ListDishTypeService().activeDishTypes();
    }

    public Dish registerDish(String name, DishType type, NutritionalInfo info, Float price) throws DataIntegrityViolationException {
        ensurePermissionOfLoggedInUser(ActionRight.ManageMenus);

        final Dish newDish = new Dish(name, type, info, price);
        final DishRepository repo = PersistenceContext.repositories().dishes();

        repo.add(newDish);

        return newDish;

    };
}
