/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.application;

import eapli.ecafeteria.domain.authz.SystemUser;
import eapli.ecafeteria.domain.mealbooking.CafeteriaUser;
import eapli.ecafeteria.persistence.CafeteriaUserRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.framework.application.Controller;



/**
 *
 * @author Miguel Ferrão
 */
public class ReturnCafeteriaUserWidthSystemUserController implements Controller{
    public CafeteriaUser returnCafeteriaUser(SystemUser user){
    final CafeteriaUserRepository cafeteriaUserRepository = PersistenceContext.repositories().cafeteriaUsers();
    
    return cafeteriaUserRepository.getCafetariaUser(user);
    }
}
