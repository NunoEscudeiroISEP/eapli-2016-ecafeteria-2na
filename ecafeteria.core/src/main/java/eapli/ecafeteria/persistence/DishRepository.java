package eapli.ecafeteria.persistence;

import eapli.ecafeteria.domain.meals.Dish;
import eapli.ecafeteria.domain.meals.DishType;
import eapli.framework.persistence.repositories.Repository;

/**
 * Created by berme on 5/12/2016.
 */
public interface DishRepository extends Repository<Dish, Long> {

    /**
     * Return all dish types of a certain type
     * @param type - DishType to use as filter
     * @return A Iterable with all dishes of a given type
     */
    Iterable<Dish> allOfType(DishType type);
}
