/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.persistence.jpa;

import eapli.ecafeteria.domain.mealbooking.*;
import eapli.ecafeteria.persistence.MovementsRepository;
import eapli.framework.persistence.DataIntegrityViolationException;
import eapli.framework.persistence.repositories.impl.jpa.JpaRepository;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;

/**
 *
 * @author Vitor Mascarenhas
 */
public class JpaMovementsRepository extends JpaRepository<Movement, Integer> implements MovementsRepository {
    
    private CriteriaBuilder cb;

    public JpaMovementsRepository()
    {
        this.cb = this.entityManager().getCriteriaBuilder();
    }
    

    @Override
    protected String persistenceUnitName() {
        return PersistenceSettings.PERSISTENCE_UNIT_NAME;
    }

    @Override
    public double getBalance(Account account) {
      CriteriaQuery query = this.createQuery();
        Root<Movement> root = query.from(Movement.class);

        Join<Movement, Account> j = root.join(Movement_.account);

        query.where(cb.equal(j.get(Account_.idAccount), account.id()));

        TypedQuery<Movement> querieResult = this.entityManager().createQuery(query);
        List<Movement> movementsList = querieResult.getResultList();
        double balance = 0;
        
        for(Movement m : movementsList){
            balance = balance + m.returnValue();
        }
        
        return balance;
    }

    /**
     *
     * @return
     */
    private CriteriaQuery createQuery()
    {
        return this.cb.createQuery(Movement.class);
    }

    /**
     *
     * @return
     */
    private List<CafeteriaUser> executeQuery(CriteriaQuery query)
    {
        TypedQuery<CafeteriaUser> querieResult = this.entityManager().createQuery(query);

        return querieResult.getResultList();
    }

    @Override
    public void addMovement(Movement m) {
        entityManager().getTransaction().begin();
        try {
            super.add(m);
        } catch (DataIntegrityViolationException ex) {
            Logger.getLogger(JpaMovementsRepository.class.getName()).log(Level.SEVERE, null, ex);
        }
        entityManager().getTransaction().commit();
    }
    
}