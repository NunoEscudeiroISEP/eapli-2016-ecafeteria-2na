package eapli.ecafeteria.persistence.jpa;

import eapli.ecafeteria.domain.authz.SystemUser;
import eapli.ecafeteria.domain.authz.SystemUser_;
import eapli.ecafeteria.domain.authz.Username;
import eapli.ecafeteria.domain.mealbooking.*;
import eapli.ecafeteria.persistence.CafeteriaUserRepository;
import eapli.framework.persistence.repositories.impl.jpa.JpaRepository;
import java.util.List;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;
import org.h2.command.dml.Query;

/**
 *
 * @author Jorge Santos ajs@isep.ipp.pt 02/04/2016
 */
class JpaCafeteriaUserRepository extends JpaRepository<CafeteriaUser, MecanographicNumber>
        implements CafeteriaUserRepository {

    private CriteriaBuilder cb;

    public JpaCafeteriaUserRepository()
    {
        this.cb = this.entityManager().getCriteriaBuilder();
    }

    @Override
    protected String persistenceUnitName() {
        return PersistenceSettings.PERSISTENCE_UNIT_NAME;
    }

    @Override
    public CafeteriaUser getCafetariaUser(SystemUser user) {

        CriteriaQuery query = this.createQuery();
        Root<CafeteriaUser> root = query.from(CafeteriaUser.class);

        Join<CafeteriaUser,SystemUser> j = root.join(CafeteriaUser_.systemUser);

        query.where(cb.equal(j.get(SystemUser_.username), user.id()));

        TypedQuery<CafeteriaUser> querieResult = this.entityManager().createQuery(query);

        try{
            return querieResult.getSingleResult();
        } catch(NoResultException e) {
            //TODO check when we have to throw message
        }

        return null;
    }

    /**
     *
     * @return
     */
    private CriteriaQuery createQuery()
    {
        return this.cb.createQuery(CafeteriaUser.class);
    }

    /**
     *
     * @return
     */
    private List<CafeteriaUser> executeQuery(CriteriaQuery query)
    {
        TypedQuery<CafeteriaUser> querieResult = this.entityManager().createQuery(query);

        return querieResult.getResultList();
    }

}
