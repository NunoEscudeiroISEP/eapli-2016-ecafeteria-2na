package eapli.ecafeteria.persistence.jpa;

import eapli.ecafeteria.domain.meals.Dish;
import eapli.ecafeteria.domain.meals.DishType;
import eapli.ecafeteria.persistence.DishRepository;
import eapli.framework.persistence.repositories.impl.jpa.JpaRepository;

/**
 * Created by berme on 5/12/2016.
 */
public class JpaDishRepository extends JpaRepository<Dish, Long> implements DishRepository {
    @Override
    protected String persistenceUnitName() {
        return PersistenceSettings.PERSISTENCE_UNIT_NAME;
    }

    @Override
    public Iterable<Dish> allOfType(DishType type) {
        return match("e.dishType.acronym='" + type.id()+"'");
    }
}
