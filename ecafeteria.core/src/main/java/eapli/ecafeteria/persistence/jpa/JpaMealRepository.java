/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.persistence.jpa;

import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.persistence.MealRepository;
import eapli.framework.persistence.repositories.impl.jpa.JpaRepository;

/**
 *
 * @author André Oliveira
 */
public class JpaMealRepository extends JpaRepository<Meal, Long> implements MealRepository {

    @Override
    protected String persistenceUnitName() {
        return PersistenceSettings.PERSISTENCE_UNIT_NAME;
    }

    @Override
    public Meal updateMeal(Meal meal) {
        entityManager().getTransaction().begin();
        Meal newMeal = this.update(meal);
        entityManager().getTransaction().commit();
        
        return newMeal;
    }
    
    @Override
    public void delete(Meal meal){
        entityManager().getTransaction().begin();
        super.delete(meal);
        entityManager().getTransaction().commit();
    }
    
}
