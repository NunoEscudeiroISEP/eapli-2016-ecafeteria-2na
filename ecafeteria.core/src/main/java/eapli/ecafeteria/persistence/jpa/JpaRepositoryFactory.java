package eapli.ecafeteria.persistence.jpa;

import eapli.ecafeteria.persistence.*;

/**
 * Created by nuno on 21/03/16.
 */
public class JpaRepositoryFactory implements RepositoryFactory {

    @Override
    public UserRepository users() {
        return new JpaUserRepository();
    }

    @Override
    public DishRepository dishes() {
        return new JpaDishRepository();
    }

    @Override
    public JpaDishTypeRepository dishTypes() {
        return new JpaDishTypeRepository();
    }

    @Override
    public JpaOrganicUnitRepository organicUnits() {
        return new JpaOrganicUnitRepository();
    }

    @Override
    public JpaCafeteriaUserRepository cafeteriaUsers() {
        return new JpaCafeteriaUserRepository();
    }

    @Override
    public SignupRequestRepository signupRequests() {
        return new JpaSignupRequestRepository();
    }

    @Override
    public BookingsRepository bookings() {
        return new JpaBookingsRepository();
    }

    @Override
    public MovementsRepository movements() {
        return new JpaMovementsRepository();
    }

    @Override
    public RegisterRepository registers() {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        return new JpaRegisterRepository();
    }

    @Override
    public MenuRepository menus() {
        return new JpaMenuRepository();
    }

    @Override
    public MealRepository meals() {
        return new JpaMealRepository();
    }

    @Override

    public AccountRepository accounts() {
        return new JpaAccountRepository();
    }

}
