/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.persistence.jpa;

import eapli.ecafeteria.domain.mealbooking.criteria.BookingCriteriaFactory;
import eapli.ecafeteria.domain.mealbooking.criteria.ICriteria;
import eapli.ecafeteria.domain.mealbooking.criteria.ICriteriaFilterValue;
import eapli.ecafeteria.domain.mealbooking.Booking;
import eapli.ecafeteria.domain.mealbooking.Booking_;
import eapli.ecafeteria.domain.meals.*;
import eapli.ecafeteria.persistence.BookingsRepository;
import eapli.framework.persistence.repositories.impl.jpa.JpaRepository;
import eapli.util.DateTime;

import java.sql.Date;
import java.util.List;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import javax.persistence.metamodel.EntityType;
import javax.persistence.metamodel.Metamodel;

import static eapli.ecafeteria.domain.meals.Dish_.*;

/**
 *
 * @author Pedro Costa
 */
public class JpaBookingsRepository extends JpaRepository<Booking, Long> 
    implements BookingsRepository
{
    private CriteriaBuilder cb;

    public JpaBookingsRepository()
    {
        this.cb = this.entityManager().getCriteriaBuilder();
    }

    /**
     *
     * @param criteria
     * @return
     */
    @Override
    public List<Booking> findByCriteria(ICriteria criteria)
    {
        CriteriaQuery query = this.decideWhere(criteria);

        return this.executeQuery(query);
    }

    /**
     *
     * @return
     */
    private CriteriaQuery createQuery()
    {
        return this.cb.createQuery(Booking.class);
    }

    /**
     *
     * @return
     */
    private List<Booking> executeQuery(CriteriaQuery query)
    {
        TypedQuery<Booking> querieResult = this.entityManager().createQuery(query);

        return querieResult.getResultList();
    }

    /**
     *
     * @param query
     * @param root
     * @param filterValue
     * @param attributeName
     * @return
     */
    private CriteriaQuery decideWhere(ICriteria criteria)
    {
        String attributeName = criteria.getCriteriaFilterName();
        ICriteriaFilterValue filterValue = criteria.getCriteriaFilter();

        CriteriaQuery query = this.createQuery();
        Root<Booking> root = query.from(Booking.class);

        Join<Booking,Meal> joinBookingMeal;
        Join <Meal,Dish> joinMealDish;
        Join<Dish,DishType> joinDishDishType;

        switch (attributeName) {

            case(BookingCriteriaFactory.FILTER_NAME_DISH_TYPE):
                joinBookingMeal = root.join(Booking_.meal,JoinType.INNER);
                joinMealDish = joinBookingMeal.join(Meal_.dish,JoinType.INNER);
                joinDishDishType = joinMealDish.join(Dish_.dishType,JoinType.INNER);
                query.where(this.cb.equal(joinDishDishType.get(DishType_.acronym), filterValue.value()));
                break;
            case(BookingCriteriaFactory.FILTER_NAME_DISH_NAME):
                joinBookingMeal = root.join(Booking_.meal,JoinType.INNER);
                joinMealDish = joinBookingMeal.join(Meal_.dish,JoinType.INNER);
                query.where(this.cb.equal(joinMealDish.get(Dish_.name), filterValue.value()));
                break;
            case(BookingCriteriaFactory.FILTER_NAME_MEAL_TYPE):
                joinBookingMeal = root.join(Booking_.meal,JoinType.INNER);
                query.where(this.cb.equal(joinBookingMeal.get(Meal_.type), filterValue.value()));
                break;
            case(BookingCriteriaFactory.FILTER_NAME_DATE_TIME):
                query.where(this.cb.equal(root.get(Booking_.bookingDate), filterValue.value()));
                break;
            default:
                throw new IllegalArgumentException("Not a valid criteria name");
        }
        return query;
    }

    @Override
    protected String persistenceUnitName()
    {
        return PersistenceSettings.PERSISTENCE_UNIT_NAME;
    }
    
}
