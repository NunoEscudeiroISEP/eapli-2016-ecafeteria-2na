/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.persistence.jpa;

import eapli.ecafeteria.domain.meals.*;
import eapli.ecafeteria.persistence.*;
import eapli.framework.persistence.repositories.impl.jpa.JpaRepository;
import java.sql.Date;
import java.util.Calendar;
import java.util.List;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;

/**
 *
 * @author soliveira
 */
public class JpaMenuRepository extends JpaRepository<Menu, Long> implements MenuRepository {

    @Override
    protected String persistenceUnitName() {
        return PersistenceSettings.PERSISTENCE_UNIT_NAME;
    }

    //TODO use state to filter
    @Override
    public Menu findByPeriod() {

        CriteriaBuilder cb = this.entityManager().getCriteriaBuilder();

        CriteriaQuery query = cb.createQuery(Menu.class);

        Root<Menu> from = query.from(Menu.class);

        Expression<Date> today = cb.currentDate();

        Join<Menu,Period> j = from.join(Menu_.period);

        Predicate and = cb.and(
            cb.lessThanOrEqualTo(j.get("startDate"),Calendar.getInstance().getTimeInMillis()),
             cb.greaterThanOrEqualTo(j.get("endDate"),Calendar.getInstance().getTimeInMillis()));

        query.where(and);


        TypedQuery<Menu> querieResult = this.entityManager().createQuery(query);

        List<Menu> menu = querieResult.getResultList();
        
        if(menu.isEmpty()){
            
            return null;
            
        }

        this.entityManager().close();

        return menu.get(0);

    }

    @Override
    public void deleteMeal(Menu menu, Meal meal) {
        entityManager().getTransaction().begin();
        entityManager().merge(menu);
        menu.meals().remove(meal);
        this.update(menu);
        entityManager().getTransaction().commit();
    }

    @Override
    public void updateMeal(Menu menu, Meal meal) {
        entityManager().getTransaction().begin();       

        entityManager().merge(menu);
        menu.meals().remove(meal);
        menu.meals().add(meal);
        
        this.update(menu);
        
        entityManager().getTransaction().commit();
    }

  

}
