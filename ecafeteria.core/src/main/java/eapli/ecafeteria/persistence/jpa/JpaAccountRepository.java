/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.persistence.jpa;

import eapli.ecafeteria.domain.mealbooking.*;
import eapli.ecafeteria.domain.meals.Menu;
import eapli.ecafeteria.domain.meals.Menu_;
import eapli.ecafeteria.domain.meals.Period;
import eapli.ecafeteria.persistence.AccountRepository;
import eapli.framework.persistence.repositories.impl.jpa.JpaRepository;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.util.List;

/**
 *
 * @author vitoralexandremascarenhasmascarenhas
 */
public class JpaAccountRepository extends JpaRepository<Account, Long> implements AccountRepository {

    private CriteriaBuilder cb;

    public JpaAccountRepository()
    {
        this.cb = this.entityManager().getCriteriaBuilder();
    }

    @Override
    protected String persistenceUnitName() {
        return null;
    }

    public Iterable<Account> findAccountByMecanographicNumber(MecanographicNumber number)
    {
        CriteriaQuery query = this.createQuery();
        Root<Account> root = query.from(Account.class);

        Join<Account,MecanographicNumber> j = root.join(Account_.mecanographicNumber);

        query.where(cb.equal(j.get(MecanographicNumber_.mecanographicNumber), number.toString()));//ToString must return only number!!

        TypedQuery<Account> querieResult = this.entityManager().createQuery(query);

        return querieResult.getResultList();
    }

    private List<Booking> executeQuery(CriteriaQuery query)
    {
        TypedQuery<Booking> querieResult = this.entityManager().createQuery(query);

        return querieResult.getResultList();
    }

    private CriteriaQuery createQuery()
    {
        return this.cb.createQuery(Account.class);
    }
}