package eapli.ecafeteria.persistence.inmemory;

import eapli.ecafeteria.domain.meals.Dish;
import eapli.ecafeteria.domain.meals.DishType;
import eapli.ecafeteria.persistence.DishRepository;
import eapli.framework.persistence.repositories.impl.inmemory.InMemoryRepository;
import java.util.ArrayList;

/**
 * Created by berme on 5/12/2016.
 */
public class InMemoryDishRepository extends InMemoryRepository<Dish, Long> implements DishRepository {

    long nextID = 1;

    @Override
    protected Long newPK(Dish entity) {
        return ++nextID;
    }

    @Override
    public Iterable<Dish> allOfType(DishType type) {
        return new ArrayList<>(); //TODO:Implementar
    }
}
