/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.persistence.inmemory;

import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.domain.meals.Menu;
import eapli.ecafeteria.domain.meals.Period;
import eapli.ecafeteria.persistence.*;
import eapli.framework.persistence.repositories.impl.inmemory.InMemoryRepository;
import java.util.Calendar;

/**
 *
 * @author soliveira
 */
public class InMemoryMenuRepository extends InMemoryRepository<Menu, Long> implements MenuRepository {

    long nextId = 1;

    @Override
    protected Long newPK(Menu entity) {
        return ++nextId;
    }

    @Override
    public Menu findByPeriod() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void deleteMeal(Menu menu, Meal meal) {
        Menu m = this.repository.get(menu.id());
        m.meals().remove(meal);
    }

    @Override
    public void updateMeal(Menu menu, Meal meal) {
        Menu m = this.repository.get(menu.id());
        m.meals().remove(meal);
        m.meals().add(meal);
    }

}
