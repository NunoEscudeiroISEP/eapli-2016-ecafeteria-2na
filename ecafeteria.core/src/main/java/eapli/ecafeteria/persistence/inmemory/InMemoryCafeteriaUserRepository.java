package eapli.ecafeteria.persistence.inmemory;

import eapli.ecafeteria.domain.authz.SystemUser;
import eapli.ecafeteria.domain.mealbooking.CafeteriaUser;
import eapli.ecafeteria.domain.mealbooking.MecanographicNumber;
import eapli.ecafeteria.persistence.CafeteriaUserRepository;
import eapli.framework.persistence.repositories.impl.inmemory.InMemoryRepository;
import java.util.Iterator;
import java.util.stream.Collectors;

/**
 *
 * @author Jorge Santos ajs@isep.ipp.pt
 * 02/04/2016
 */
public class InMemoryCafeteriaUserRepository extends InMemoryRepository<CafeteriaUser, MecanographicNumber> implements CafeteriaUserRepository {
    
   

    @Override
    public CafeteriaUser getCafetariaUser(SystemUser user) {
        
        Iterable<CafeteriaUser> it = all();
        while(it.iterator().hasNext()){
            if(it.iterator().next().compareUsers(user) == true){
                return it.iterator().next();
            }
        }
        return null;
    }
    
    @Override
    public CafeteriaUser save(CafeteriaUser entity) {
        this.repository.put(newPK(entity), entity);
        return entity;
    }
    
    @Override
    public Iterable<CafeteriaUser> all() {
        return this.repository.values();
    }

    @Override
    protected MecanographicNumber newPK(CafeteriaUser entity) {
        return entity.mecanographicNumber();
    }
}
