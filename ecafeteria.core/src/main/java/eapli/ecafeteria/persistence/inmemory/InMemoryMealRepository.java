/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.persistence.inmemory;

import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.persistence.MealRepository;
import eapli.framework.persistence.repositories.impl.inmemory.InMemoryRepository;

/**
 *
 * @author André Oliveira
 */
public class InMemoryMealRepository extends InMemoryRepository<Meal,Long>  implements MealRepository {
    
    long nextId =1;

    @Override
    protected Long newPK(Meal entity) {
        return ++nextId;
    }

    @Override
    public Meal updateMeal(Meal meal) {
        
        //atualizar o existente
        return this.repository.put(meal.id(), meal);
  
    }
    
}
