/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.persistence.inmemory;

import eapli.ecafeteria.domain.register.Register;
import eapli.ecafeteria.persistence.RegisterRepository;
import eapli.framework.persistence.repositories.impl.inmemory.InMemoryRepository;

/**
 *
 * @author guest
 */
public class InMemoryRegisterRepository extends InMemoryRepository<Register, Long> 
        implements RegisterRepository {

    long nextID = 1;

    @Override
    protected Long newPK(Register entity) {
        return ++nextID;
    }   
    
}
