package eapli.ecafeteria.persistence.inmemory;

import eapli.ecafeteria.persistence.*;

/**
 *
 * Created by nuno on 20/03/16.
 */
public class InMemoryRepositoryFactory implements RepositoryFactory {

    private static UserRepository userRepository = null;
    private static DishRepository dishRepository = null;
    private static DishTypeRepository dishTypeRepository = null;
    private static OrganicUnitRepository organicUnitRepository = null;
    private static CafeteriaUserRepository cafeteriaUserRepository = null;
    private static SignupRequestRepository signupRequestRepository = null;
    private static MovementsRepository movementsRepository = null;
    private static RegisterRepository registerRepository = null;
    private static MenuRepository menusRepository = null;
    private static MealRepository mealsRepository = null;
    private static AccountRepository accountsRepository = null;

private static BookingsRepository bookingsRepository = null;


    @Override
    public UserRepository users() {
        if (userRepository == null) {
            userRepository = new InMemoryUserRepository();
        }
        return userRepository;
    }

    @Override
    public DishRepository dishes() {
        if (dishRepository == null) {
            dishRepository = new InMemoryDishRepository();
        }
        return dishRepository;
    }

    @Override
    public DishTypeRepository dishTypes() {
        if (dishTypeRepository == null) {
            dishTypeRepository = new InMemoryDishTypeRepository();
        }
        return dishTypeRepository;
    }

    @Override
    public OrganicUnitRepository organicUnits() {
        if (organicUnitRepository == null) {
            organicUnitRepository = new InMemoryOrganicUnitRepository();
        }
        return organicUnitRepository;
    }

    @Override
    public CafeteriaUserRepository cafeteriaUsers() {

        if (cafeteriaUserRepository == null) {
            cafeteriaUserRepository = new InMemoryCafeteriaUserRepository();
        }
        return cafeteriaUserRepository;
    }
    
    @Override
    public SignupRequestRepository signupRequests() {

        if (signupRequestRepository == null) {
            signupRequestRepository = new InMemorySignupRequestRepository();
        }
        return signupRequestRepository;
    }

    @Override
    public BookingsRepository bookings()
    {
        if (bookingsRepository == null) {
            bookingsRepository = new InMemoryBookingsRepository();
        }
        return bookingsRepository;
    }

    @Override
    public MovementsRepository movements() {
        
        if (movementsRepository == null) {
            movementsRepository = new InMemoryMovementsRepository();
        }
        return movementsRepository;
    }
    
    @Override
    public RegisterRepository registers() {
        
        if (registerRepository == null) {
            registerRepository = new InMemoryRegisterRepository();
        }
        return registerRepository;
    }

    @Override
    public MenuRepository menus() {
        if (menusRepository == null) {
            menusRepository = new InMemoryMenuRepository();
        }
        return menusRepository;
    }

    @Override
    public MealRepository meals() {
         if (mealsRepository == null) {
            mealsRepository = new InMemoryMealRepository();
        }
        return mealsRepository;  
    }

    @Override
    public AccountRepository accounts() {
         if (accountsRepository == null) {
            accountsRepository = new InMemoryAccountRepository();
        }
        return accountsRepository;  
    }
}
