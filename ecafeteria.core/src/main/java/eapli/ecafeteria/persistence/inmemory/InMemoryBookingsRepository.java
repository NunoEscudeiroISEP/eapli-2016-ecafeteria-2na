/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.persistence.inmemory;

import eapli.ecafeteria.domain.mealbooking.criteria.*;
import eapli.ecafeteria.domain.mealbooking.Booking;
import eapli.ecafeteria.persistence.BookingsRepository;
import eapli.framework.persistence.repositories.impl.inmemory.InMemoryRepository;
import java.util.List;
import java.util.stream.Collectors;

/**
 *
 * @author Pedro Costa
 */
public class InMemoryBookingsRepository extends InMemoryRepository<Booking,Long>
    implements BookingsRepository
{
    long nextId =1;

    @Override
    public List<Booking> findByCriteria(ICriteria criteria)
    {
        switch(criteria.getCriteriaFilterName())
        {
            case BookingCriteriaFactory.FILTER_NAME_DATE_TIME:

                CriteriaFilterValueDateTime filterDate = (CriteriaFilterValueDateTime) criteria.getCriteriaFilter();
                return repository.values().stream().filter(e -> e.sameBookingDate(filterDate.value())).collect(Collectors.toList());

            case BookingCriteriaFactory.FILTER_NAME_MEAL_TYPE:

                CriteriaFilterValueMealType filterMeal = (CriteriaFilterValueMealType) criteria.getCriteriaFilter();
                return repository.values().stream().filter(e -> e.sameMealType(filterMeal.value())).collect(Collectors.toList());

            case BookingCriteriaFactory.FILTER_NAME_DISH_TYPE:

                CriteriaFilterValueString filterStr = (CriteriaFilterValueString) criteria.getCriteriaFilter();
                return repository.values().stream().filter(e -> e.sameDishType(filterStr.value())).collect(Collectors.toList());

            case BookingCriteriaFactory.FILTER_NAME_DISH_NAME:
                CriteriaFilterValueString filterDish = (CriteriaFilterValueString) criteria.getCriteriaFilter();
                return repository.values().stream().filter( e -> e.sameDishName(filterDish.value())).collect(Collectors.toList());
        }
        
        throw new IllegalArgumentException("Invalid filter Name");
    }

    @Override
    protected Long newPK(Booking entity)
    {
        return ++nextId;
    }
    
}
