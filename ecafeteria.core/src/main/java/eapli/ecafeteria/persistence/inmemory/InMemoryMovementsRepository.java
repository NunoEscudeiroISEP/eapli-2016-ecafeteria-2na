/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.persistence.inmemory;

import com.sun.javafx.scene.control.skin.VirtualFlow;
import eapli.ecafeteria.domain.mealbooking.Account;
import eapli.ecafeteria.domain.mealbooking.Movement;
import eapli.ecafeteria.persistence.MovementsRepository;
import eapli.framework.persistence.repositories.impl.inmemory.InMemoryRepository;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


/**
 *
 * @author Vitor Mascarenhas
 */
public class InMemoryMovementsRepository extends InMemoryRepository<Movement, Integer> implements MovementsRepository{
    long nextId = 1;
    
    @Override
    public double getBalance(Account account) {
        double total = this.repository.values().stream()
                .filter(e -> e.getAccountId().equalsIgnoreCase(account.toString()))
                    .mapToDouble(Movement::returnValue).sum();

        return total;
    }
    
    @Override
    public Movement save(Movement entity) {
        this.repository.put(newPK(entity), entity);
        return entity;
    }
    
    @Override
    public Iterable<Movement> all() {
        return this.repository.values();
    }

    @Override
    protected Integer newPK(Movement entity) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void addMovement(Movement m) {
        this.repository.put(Integer.SIZE, m);
        updateBalance(m.showAccount());
    }
    
    private void updateBalance(Account account) {
        
        List<Movement> listMovs = new ArrayList<>();
        
        //separa todos os movimentos de uma conta
        for(int i = 0; i < this.repository.size();i++) {
            if(this.repository.get(i).getAccountId().equals(account.toString())) {
                listMovs.add(this.repository.get(i));
            }
        }
        
        double balance = 0;
        
        //soma todos os movimentos
        for(Movement m : listMovs) {
            balance+=m.returnValue();
        }
        
        //atualiza o saldo
        account.updateBalance(balance);
    }
}