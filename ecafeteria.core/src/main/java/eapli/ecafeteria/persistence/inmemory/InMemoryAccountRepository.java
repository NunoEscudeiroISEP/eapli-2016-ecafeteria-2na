/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.persistence.inmemory;

import eapli.ecafeteria.domain.mealbooking.Account;
import eapli.ecafeteria.domain.mealbooking.MecanographicNumber;
import eapli.ecafeteria.persistence.AccountRepository;
import eapli.framework.persistence.repositories.impl.inmemory.InMemoryRepository;
import java.util.stream.Collectors;

/**
 *
 * @author vitoralexandremascarenhasmascarenhas
 */
public class InMemoryAccountRepository extends InMemoryRepository<Account, Long> implements AccountRepository {


    @Override
    protected Long newPK(Account entity) {
        return null;
    }

    @Override
    public Account findById(Long id) {
        return null;
    }

    @Override
    public Iterable<Account> findAccountByMecanographicNumber(MecanographicNumber number)
    {
        return repository.values().stream().filter(e -> e.owner().equals(number)).collect(Collectors.toList());
    }
}