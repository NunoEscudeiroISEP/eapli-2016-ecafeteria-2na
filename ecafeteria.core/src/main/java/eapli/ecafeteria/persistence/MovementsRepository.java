/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.persistence;

import eapli.ecafeteria.domain.mealbooking.Account;
import eapli.ecafeteria.domain.mealbooking.Movement;
import eapli.framework.persistence.repositories.Repository;


/**
 *
 * @author Vitor Mascarenhas
 */
public interface MovementsRepository extends Repository<Movement, Integer>{

    public void addMovement(Movement m);
    public double getBalance(Account account);

}