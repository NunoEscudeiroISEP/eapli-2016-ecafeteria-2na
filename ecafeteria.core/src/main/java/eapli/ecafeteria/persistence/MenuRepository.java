/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.persistence;

import eapli.ecafeteria.domain.meals.*;
import eapli.framework.persistence.repositories.Repository;

/**
 *
 * @author soliveira
 */
public interface MenuRepository extends Repository<Menu, Long> {
    
    
    public Menu findByPeriod();

    public void deleteMeal(Menu menu, Meal meal);
    
    public void updateMeal(Menu menu, Meal meal);
}
