/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.persistence;

import eapli.ecafeteria.domain.mealbooking.criteria.ICriteria;
import eapli.ecafeteria.domain.mealbooking.Booking;
import eapli.ecafeteria.domain.meals.Dish;
import eapli.framework.persistence.repositories.Repository;
import java.util.List;

/**
 *
 * @author Pedro Costa
 */
public interface BookingsRepository extends Repository<Booking, Long>
{
    
    List<Booking> findByCriteria(ICriteria criteria);

    
}
