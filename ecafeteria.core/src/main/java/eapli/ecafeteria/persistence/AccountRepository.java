/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.persistence;


import eapli.ecafeteria.domain.mealbooking.Account;
import eapli.ecafeteria.domain.mealbooking.MecanographicNumber;
import eapli.framework.persistence.repositories.Repository;

/**
 *
 * @author vitoralexandremascarenhasmascarenhas
 */
public interface AccountRepository extends Repository<Account, Long> {


    public Iterable<Account> findAccountByMecanographicNumber(MecanographicNumber number);
}