/**
 *
 */
package eapli.ecafeteria.persistence;

/**
 * @author Paulo Gandra Sousa
 */
public interface RepositoryFactory {

    UserRepository users();

    DishRepository dishes();

    DishTypeRepository dishTypes();

    OrganicUnitRepository organicUnits();

    CafeteriaUserRepository cafeteriaUsers();

    SignupRequestRepository signupRequests();

    MovementsRepository movements();

    BookingsRepository bookings();
    
    RegisterRepository registers();
    
    MenuRepository menus();
    
    MealRepository meals();

    AccountRepository accounts();

}
