/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.domain.cafeteria;

import eapli.framework.domain.Factory;

/**
 *
 * @author vitoralexandremascarenhasmascarenhas
 */
public class OrganicUnitBuilder implements Factory<OrganicUnit> {
    
    private String acronym;
    private String name;
    private String description;

    public OrganicUnitBuilder() {
    }
    
    public OrganicUnitBuilder withAcronym(String acronym) {
        this.acronym = acronym;
        return this;
    }                
    
    public OrganicUnitBuilder withName(String name) {
        this.name = name;
        return this;
    }

    public OrganicUnitBuilder withDescription(String description) {
        this.description = description;
        return this;
    }

    @Override
    public OrganicUnit build() {
        return new OrganicUnit(acronym, name, description);
    }
}