/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.domain.meals;

import eapli.framework.domain.AggregateRoot;
import java.io.Serializable;
import java.util.*;
import javax.persistence.*;

/**
 *
 * @author soliveira
 */
@Entity
public class Menu implements AggregateRoot<Long>, Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue
    @Column(name="MENU_ID")
    private Long id;

    @Embedded
    private Period period;
    private MenuStatus status;

    @OneToMany (cascade = CascadeType.PERSIST, fetch = FetchType.LAZY, targetEntity = Meal.class)
    @JoinColumn(name = "OWNER_MENU_ID", referencedColumnName = "MENU_ID")
    private Set<Meal> meals;

    protected Menu() {
    }

    public Menu(Period period, Set<Meal> meals) {

        if(period == null || meals == null){
            throw new IllegalArgumentException("Period and/or meals cannot be null");
        }

        this.meals = meals;
        this.period = period;
        this.status = MenuStatus.CREATED;
    }

    @Override
    public boolean sameAs(Object other) {
        return Objects.equals(this, other);
    }

    @Override
    public boolean is(Long id) {
        return Objects.equals(this.id, id);
    }

    @Override
    public Long id() {
        return this.id;
    }
    
    public MenuStatus status(){
        return this.status;
    }
    
    public Period period(){
        return this.period;
    }
    
    public Set<Meal> meals(){
        return this.meals;
    }
    
    public List<Meal> mealOfType(DishType dishType){
        
        List<Meal> dishesList = new ArrayList<>();
        
        for (Meal meal : this.meals) {
            if(meal.sameDishType(dishType.id())){
                dishesList.add(meal);
            }
        }
        
        return dishesList;
        
    }
    
    public void defineNewPeriod(Period period){
        this.period = period;
    }

    @Override
    public String toString() {
        return "Menu{" + "id=" + id + ", period=" + period + ", meals=" + meals + 
                ", status=" + status + '}';
    }
    
}

