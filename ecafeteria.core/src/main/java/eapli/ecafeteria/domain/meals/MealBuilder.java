/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.domain.meals;

import eapli.framework.domain.Factory;
import java.util.Calendar;

/**
 *
 * @author soliveira
 */
public class MealBuilder implements Factory<Meal> {
    
    private Calendar workingDate;
    private final Period period;
    private MealType mealType;
    private int numberOfDishes;
    private Dish dish;
    
    public MealBuilder(Period period)
    {
        this.period = period;
    }
    
    public MealBuilder forMealType(MealType mealType){
        this.mealType = mealType;
        
        return this;
    }
    
    public MealBuilder forWeekDay(int weekDay){
        
        workingDate = (Calendar)this.period.startDate().clone();
        
        switch (weekDay) {
            case 1:
                break;
            case 7:
                workingDate = this.period.endDate();
                break;
            default:
                workingDate.add(Calendar.DATE, weekDay - 1);
                break;
        } 
        
        return this;
    }
    
    public MealBuilder withNumberOfDishes(int numberOfDishes){
        this.numberOfDishes = numberOfDishes;
        return this;
    }
    
    public MealBuilder withDish(Dish dish){
        this.dish = dish;
        return this;
    }

    @Override
    public Meal build() {
        return new Meal(dish, mealType, workingDate, numberOfDishes);
    }
    
}
