/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.domain.meals;

/**
 *
 * @author ASUS
 */
public enum MenuStatus {
    CREATED,
    PUBLISHED;
    
    /**
     * Convert a string to respective enum.
     * 
     * @param type String associated with enum
     * 
     * @return Enum associated or null if not found.
     */
    public static MenuStatus menuStatusFromString(String type)
    {
        type = type.toLowerCase().trim();
        
        switch(type){
            case "created":
                return CREATED;
            case "published":
                return PUBLISHED;
        }
        return null;
    }    
}

