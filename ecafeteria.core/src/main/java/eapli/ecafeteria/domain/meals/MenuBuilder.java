/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.domain.meals;

import eapli.framework.domain.Factory;
import eapli.util.DateTime;
import java.util.Calendar;
import java.util.*;

/**
 *
 * @author soliveira
 */
public class MenuBuilder implements Factory<Menu> {

    private Period period;
    private final Set<Meal> meals;

    public MenuBuilder() {
        meals = new HashSet<>();
    }

    public MenuBuilder referringToWeek(int weekNumber) {
        Calendar startDate = DateTime.beginningOfWeek(DateTime.currentYear(), weekNumber); //TODO: verificar a possibilidade de deixar escolher outro mais anos
        Calendar endDate = DateTime.endOfWeek(DateTime.currentYear(), weekNumber);

        period = new Period(startDate, endDate);

        return this;
    }
    
    public MenuBuilder withMeal(Meal meal){
        meals.add(meal);        
        return this;
    }

    public Period getPeriod(){
        return this.period;
    }
    
    @Override
    public Menu build() {
        return new Menu(period,meals);
    }

}
