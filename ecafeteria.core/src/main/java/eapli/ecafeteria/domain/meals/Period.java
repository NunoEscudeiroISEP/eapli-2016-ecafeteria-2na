/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.domain.meals;

import eapli.framework.domain.ValueObject;
import eapli.util.DateTime;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import javax.persistence.*;


/**
 *
 * @author soliveira
 */
@Embeddable
public class Period implements Serializable, ValueObject {

    private static final long serialVersionUID = 1L;

    @Temporal(TemporalType.DATE)
    @Column(unique = true)
    private Calendar startDate;
    @Temporal(TemporalType.DATE)
    @Column(unique = true)
    private Calendar endDate;


    protected Period() {
    }

    /**
     * Cria o objeto periodo com o "tempo de duracao" da ementa Considera a
     * semana 1 do ano, a primeira semana completa A semana começa no Domingo
     *
     * @param startDate data de inicio do periodo
     * @param endDate data de fim do periodo
     */
    public Period(Calendar startDate, Calendar endDate) {
        startDate.add(Calendar.DATE, -1); //força o domingo como primeiro dia da semana
        this.startDate = startDate;

        endDate.add(Calendar.DATE, -1);
        this.endDate = endDate;
    }
    
    public Period(int weekNumber){
        Calendar startDate = DateTime.beginningOfWeek(DateTime.currentYear(), weekNumber); //TODO: verificar a possibilidade de deixar escolher outro mais anos
        Calendar endDate = DateTime.endOfWeek(DateTime.currentYear(), weekNumber);

        startDate.add(Calendar.DATE, -1); //força o domingo como primeiro dia da semana
        this.startDate = startDate;

        endDate.add(Calendar.DATE, -1);
        this.endDate = endDate;
    }

    /**
     * @return the startDate
     */
    public Calendar startDate() {
        return startDate;
    }

    /**
     * @return the endDate
     */
    public Calendar endDate() {
        return endDate;
    }

    @Override
    public String toString() {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        return String.format("From %s to %s", format.format(this.startDate.getTime()), format.format(this.endDate.getTime()) );
    }
}
