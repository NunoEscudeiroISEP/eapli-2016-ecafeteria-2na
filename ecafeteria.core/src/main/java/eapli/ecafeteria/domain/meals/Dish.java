package eapli.ecafeteria.domain.meals;

import eapli.framework.domain.AggregateRoot;
import eapli.util.Strings;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by berme on 5/12/2016.
 */
@Entity
public class Dish implements AggregateRoot<String>, Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue
    private Long id;

    @Column(unique = true)
    private String name;
    @OneToOne
    private DishType dishType;
    private NutritionalInfo nutritionalInfo;
    private Float price;

    private Dish() {
        // for ORM
    }

    public Dish(String name, DishType dishType, NutritionalInfo nutritionalInfo, Float price) {
        if (Strings.isNullOrEmpty(name) || dishType == null || nutritionalInfo == null || price == null || price < 0) {
            throw new IllegalArgumentException();
        }

        this.name = name;
        this.dishType = dishType;
        this.nutritionalInfo = nutritionalInfo;
        this.price = price;
    }

    public String name() {
        return this.name;
    }

    public DishType dishType() {
        return dishType;
    }

    public NutritionalInfo nutritionalInfo() {
        return nutritionalInfo;
    }

    public Float price() {
        return price;
    }

    @Override
    public boolean sameAs(Object other) {
        if (this == other ) {
            return true;
        }
        if (other  == null || this.getClass() != other .getClass()) {
            return false;
        }
        Dish otherDish = (Dish) other;

        return otherDish.is(id());
    }

    @Override
    public boolean is(String id) {
        return id.equalsIgnoreCase(id());
    }

    @Override
    public String id() {
        return name();
    }

    public Boolean sameType(String otherType) {
        return this.dishType.sameAcronym(otherType);
    }

    public Boolean sameName(String otherName) {
        return this.name.equalsIgnoreCase(otherName);
    }
}
