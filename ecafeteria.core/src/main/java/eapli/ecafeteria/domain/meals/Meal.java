/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.domain.meals;

import eapli.framework.domain.AggregateRoot;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Objects;
import javax.persistence.*;

/**
 *
 * @author soliveira
 */
@Entity
public class Meal implements AggregateRoot<Long>, Serializable {

    @Id
    @GeneratedValue
    @Column(name = "MEAL_ID")
    private Long id;
    @OneToOne
    private Dish dish;
    private MealType type;
    @Temporal(TemporalType.DATE)
    private Calendar date;

    private int availableDishes; //número de refeições disponíveis deste prato

    private int numberOfDishes;
    private int numberOfConfectionDishes;
    private int numberOfNotSoldDishes;

    protected Meal() {
    }

    public Meal(Dish dish, MealType type, Calendar date, int numberOfDishes) {
        this.dish = dish;
        this.type = type;
        this.date = date;
        this.numberOfDishes = numberOfDishes;
        this.availableDishes = this.numberOfDishes;
        this.numberOfConfectionDishes = 0;
        this.numberOfNotSoldDishes = 0;
    }

    @Override
    public boolean sameAs(Object other) {
        return Objects.equals(this, other);
    }

    @Override
    public boolean is(Long id) {
        return Objects.equals(this.id, id);
    }

    @Override
    public Long id() {
        return this.id;
    }

    public Boolean sameMealType(MealType otherType) {
        return this.type.equals(otherType);
    }

    public Boolean sameDishType(String otherDishType) {
        return this.dish.sameType(otherDishType);
    }

    public Boolean sameDishName(String otherDishName) {
        return this.dish.sameName(otherDishName);
    }

    public void numberOfConfectionMeals(int confectionMeals) {

        this.numberOfConfectionDishes = confectionMeals;

    }
    
    public void numberOfNotSoldMeals() {

        this.numberOfNotSoldDishes = this.numberOfDishes - this.numberOfConfectionDishes;

    }

    public String dishName() {
        return this.dish.name();
    }

    public DishType dishType() {
        return this.dish.dishType();
    }

    public String formatedMealDate() {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        return format.format(this.date.getTime());
    }

    public Calendar mealDate() {
        return this.date;
    }

    public MealType mealType() {
        return this.type;
    }

    public int numberOfDishes() {
        return this.numberOfDishes;
    }

    //Neste momento considera-se que o preço da refeição é o preço do prato
    public float price(){
        return this.dish.price();
    }
  
    public void changeNumberOfDishes(int newNumberOfDishes) {
        this.numberOfDishes = newNumberOfDishes;
    }
    
    public void changeMealType(MealType type){
        this.type = type;
    }
    
    public void decrementAvailableDishes(){
        this.availableDishes--;
    }

    @Override
    public String toString() {

        StringBuilder str = new StringBuilder();
        str.append("Dish: ");
        str.append(this.dishName());
        str.append("\nMeal type: ");
        str.append(this.mealType());
        str.append("\n");

        return str.toString();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        
        if(obj == null || obj.getClass() != this.getClass()){
            return false;
        }
        
        return this.is(((Meal)obj).id());
    }

    //usado apenas para testes unitários
    int getNumberOfConfectionDishes() {
        return this.numberOfConfectionDishes;
    }
    int getNumberOfNotSoldDishes() {
        return this.numberOfNotSoldDishes;
    }

    public boolean isAvailable() {
        return this.availableDishes > 0;
    }
}
