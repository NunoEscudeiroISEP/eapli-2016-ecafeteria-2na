/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.domain.meals;

import java.sql.Date;
import java.util.Calendar;

import javax.persistence.Entity;

/**
 *
 * @author soliveira
 */
public enum MealType
{

    BREAKFAST(-1), //não definida hora limite para marcação de pequeno-almoço
    LUNCH(10),
    DINNER(16);

    private int limitTime;

    private MealType(int limitTime)
    {
        this.limitTime = limitTime;
    }

    /**
     * Convert a string to respective enum.
     *
     * @param type String associated with enum
     *
     * @return Enum associated or null if not found.
     */
    public static MealType mealTypeFromString(String type)
    {
        type = type.toLowerCase().trim();

        switch (type) {
            case "breakfast":
                return BREAKFAST;
            case "lunch":
                return LUNCH;
            case "dinner":
                return DINNER;
        }
        return null;
    }

    //
    /**
     * Verifica se o utilizador pode efetuar a reserva, tendo em conta as
     * restrições temporais: reservas para almoço até às 10h e reservas para o
     * jantar até às 16h.
     *
     * @param meal - refeição que se pretende efetuar reserva
     * @param bookingDate data da marcação da refeição
     * @return true se já houver reserva prévia, false, caso contrário
     */
    public boolean checkTime(Meal meal, Date bookingDate)
    {   
        //data limite para a marcação da refeição
        Calendar limitDate = limitDate(meal.mealDate());
        return (limitDate.after(meal.mealDate()));
    }

    private Calendar limitDate(Calendar mealDate)
    {
        int year = mealDate.get(Calendar.YEAR);
        int month = mealDate.get(Calendar.MONTH); // 0 -January
        int date = mealDate.get(Calendar.DAY_OF_MONTH);

        Calendar limitDate = Calendar.getInstance();
        limitDate.clear();

        limitDate.set(Calendar.YEAR, year);
        limitDate.set(Calendar.MONTH, month);
        limitDate.set(Calendar.DATE, date);
        limitDate.set(Calendar.HOUR, limit());
        limitDate.set(Calendar.MINUTE, 0);
        limitDate.set(Calendar.SECOND, 0);
        
        return limitDate;
    }

    private int limit()
    {
        return limitTime;
    }

}
