package eapli.ecafeteria.domain.meals;

import java.io.Serializable;
import javax.persistence.Embeddable;

/**
 * Created by berme on 5/12/2016.
 */
@Embeddable
public class NutritionalInfo implements Serializable {
    //Calories in Kcal
    private Integer calories;

    //Salt quantity in grams
    private Float salt;

    //Fats quantity in grams
    private Float fats;

    private NutritionalInfo() {
    }

    public NutritionalInfo(Integer calories, Float salt, Float fats) {
        if (calories == null || salt == null || fats == null || calories < 0
                || salt < 0 || fats < 0) {
            throw new IllegalArgumentException();
        }

        this.calories = calories;
        this.salt = salt;
        this.fats = fats;
    }
}
