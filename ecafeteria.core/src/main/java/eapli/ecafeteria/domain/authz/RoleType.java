package eapli.ecafeteria.domain.authz;


public enum RoleType {
    User, Utente,
    Admin, KitchenManager, MenuManager, Cashier
}
