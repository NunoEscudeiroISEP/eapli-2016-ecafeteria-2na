/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.domain.mealbooking.criteria;

/**
 *
 * @author Pedro Costa
 */
public class BookingCriteria implements ICriteria
{
    private final String criteriaName;
    private final ICriteriaFilterValue criteriaValue;
    
    public BookingCriteria(String criteriaName, ICriteriaFilterValue criteriaValue)
    {
        this.criteriaName = criteriaName;
        this.criteriaValue = criteriaValue;
    }

    @Override
    public ICriteriaFilterValue getCriteriaFilter()
    {
        return this.criteriaValue;
    }

    @Override
    public String getCriteriaFilterName()
    {
        return this.criteriaName;
    }

}
