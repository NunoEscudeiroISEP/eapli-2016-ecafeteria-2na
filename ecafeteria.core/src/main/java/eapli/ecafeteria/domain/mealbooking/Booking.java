/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.domain.mealbooking;

import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.domain.meals.MealType;
import eapli.framework.domain.AggregateRoot;
import eapli.framework.domain.Money;
import eapli.util.DateTime;

import java.io.Serializable;
import java.sql.Date;
import java.util.Calendar;
import javax.persistence.*;

/**
 *
 * @author Pedro Costa
 */
@Entity
public class Booking implements AggregateRoot<Long>, Serializable
{
    @Id
    @GeneratedValue
    private long id;
    @OneToOne
    private Meal meal;
    private Date bookingDate;
    private static final long serialVersionUID = 1L;
    private MecanographicNumber mecanographicUserNumber;
    
    protected Booking()
    {
    }
    
    public Booking(Meal meal, Date reservationDate, MecanographicNumber mecanographicUserNumber)
    {
        this.meal = meal;
        this.bookingDate = reservationDate;
        this.mecanographicUserNumber = mecanographicUserNumber;
    }

    @Override
    public boolean sameAs(Object other)
    {
        if (this == other) {
            return true;
        }
        if (other == null || this.getClass() != other.getClass()) {
            return false;
        }
        
        Booking otherReservation = (Booking) other;
        
        return otherReservation.id().equals(this.id());
    }

    @Override
    public boolean is(Long id)
    {
        return id().equals(id);
    }

    @Override
    public Long id()
    {
        return this.id;
    }
    
    public boolean sameBookingDate(Date otherDate)
    {
        return this.bookingDate.equals(otherDate);
    }
    
    public Boolean sameMealType(MealType otherType)
    {
        return this.meal.sameMealType(otherType);
    }
    
    public Boolean sameDishType(String otherDishType)
    {
        return this.meal.sameDishType(otherDishType);
    }
    
    public Boolean sameDishName(String otherDishName)
    {
        return this.meal.sameDishName(otherDishName);
    }

    @Override
    public String toString()
    {
        StringBuilder str = new StringBuilder();
        str.append("\nBooking: ");
        str.append(this.id);

        str.append("\nDate: ");
        str.append(this.bookingDate);

        str.append(this.meal);


        return str.toString();
    }
}
