/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.domain.mealbooking.criteria;

import java.io.Serializable;
import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;

/**
 *
 * @author Pedro Costa
 */
public class CriteriaFilterValueDateTime implements ICriteriaFilterValue<Date>, Serializable
{
    private final Date bookingDate;
    
    public CriteriaFilterValueDateTime(String value)
    {
        this.bookingDate = java.sql.Date.valueOf(value);
    }

    @Override
    public Date value()
    {
        return this.bookingDate;
    }
}
