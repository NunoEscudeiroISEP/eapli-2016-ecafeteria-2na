/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.domain.mealbooking;

import java.io.Serializable;

/**
 *
 * @author Pedro Costa
 */
public class Price implements Serializable
{
    private double bookingPrice;

    protected  Price()
    {

    }

    public Price(Double bookingPrice)
    {
        this.bookingPrice = bookingPrice;
    }
}
