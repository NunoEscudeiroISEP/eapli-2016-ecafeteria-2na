/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.domain.mealbooking;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.Date;

/**
 *
 * @author Vitor Mascarenhas
 */
@Entity
public class Movement {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue
    private int id;
    private Account account;
    private Date date;
    private String description;
    private double value;
    
    public Movement() {
    }
    
    public Movement(Account account, Date date, String description, double value) {
        this.account = account;
        this.date = date;
        this.description = description;
        this.value = value;
    }
    
    public String getAccountId(){
        return this.account.toString();
    }
    
    public double returnValue(){
        return this.value;
    }
    
    public Account showAccount() {
        return this.account;
    }
}