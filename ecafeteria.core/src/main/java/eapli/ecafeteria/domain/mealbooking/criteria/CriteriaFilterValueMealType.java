/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.domain.mealbooking.criteria;

import eapli.ecafeteria.domain.meals.MealType;
import java.io.Serializable;

/**
 *
 * @author Pedro Costa
 */
public class CriteriaFilterValueMealType implements ICriteriaFilterValue<MealType>, Serializable
{
    private final MealType value;
    
    public CriteriaFilterValueMealType(String value)
    {
        this.value = MealType.mealTypeFromString(value);
    }

    @Override
    public MealType value()
    {
        return this.value;
    }
}
