/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates and open the template
 * in the editor.
 */
package eapli.ecafeteria.domain.mealbooking;

import static eapli.ecafeteria.AppSettings.ensurePermissionOfLoggedInUser;
import eapli.ecafeteria.domain.authz.ActionRight;
import eapli.ecafeteria.persistence.MovementsRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import java.io.Serializable;

import javax.persistence.Embeddable;

import eapli.framework.domain.ValueObject;
import eapli.framework.persistence.DataIntegrityViolationException;
import eapli.util.Strings;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 *
 * @author Jorge Santos ajs@isep.ipp.pt
 */
@Entity
public class Account implements ValueObject, Serializable {

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private int idAccount;
    private static final long serialVersionUID = 1L;
    @Column(unique = true)
    private MecanographicNumber mecanographicNumber;
    private String account;
    private double balance;
    
    
    public Account(String account, MecanographicNumber mecanographicNumber) {
        if (Strings.isNullOrEmpty(account) || Strings.isNullOrEmpty(mecanographicNumber.toString())) {
            throw new IllegalStateException("Account should neither be null nor empty");
        } else if (!mecanographicNumber.toString().matches("[0-9]{7}")) {
            throw new IllegalStateException("Wrong mecanographic number format");
        }
        
        this.account = account;
        this.mecanographicNumber = mecanographicNumber;
        this.balance = 0;
    }

    protected Account() {
        // for ORM
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Account)) {
            return false;
        }

        final Account that = (Account) o;

        return this.account.equals(that.account);

    }

    @Override
    public int hashCode() {
        return this.account.hashCode();
    }

    @Override
    public String toString() {
        return this.account;
    }
    
    
    public double getBalance() throws DataIntegrityViolationException {
        final MovementsRepository repo = PersistenceContext.repositories().movements();
        return repo.getBalance(this);
    }
    
    public void updateBalance(double balance) {
        this.balance = balance;
    }


    public MecanographicNumber owner() {return this.mecanographicNumber;}

    public int id()
    {
        return this.idAccount;
    }
}
