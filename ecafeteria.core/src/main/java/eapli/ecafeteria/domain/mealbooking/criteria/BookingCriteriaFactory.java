/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.domain.mealbooking.criteria;

import eapli.util.DateTime;

import java.util.Calendar;

/**
 *
 * @author Pedro Costa
 */
public class BookingCriteriaFactory implements ICriteriaFactory
{
    public static final String FILTER_NAME_MEAL_TYPE="mealType";
    public static final String FILTER_NAME_DATE_TIME="bookingDate";
    public static final String FILTER_NAME_DISH_TYPE="dishType";
    public static final String FILTER_NAME_DISH_NAME="dishName";

    @Override
    public ICriteria generateBookingCriteria(String criteriaFilterName, String criteriaFilterValue)
    {
        ICriteriaFilterValue criteriaValue;
        
        switch(criteriaFilterName){
            
            case FILTER_NAME_MEAL_TYPE:
                criteriaValue = new CriteriaFilterValueMealType(criteriaFilterValue);
                return new BookingCriteria(criteriaFilterName,criteriaValue);
            case FILTER_NAME_DATE_TIME:
                criteriaValue = new CriteriaFilterValueDateTime(criteriaFilterValue);
                return new BookingCriteria(criteriaFilterName,criteriaValue);
            default:
                criteriaValue = new CriteriaFilterValueString(criteriaFilterValue);
                return new BookingCriteria(criteriaFilterName,criteriaValue);
        }
    }
}
