/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.domain.mealbooking.criteria;

import java.io.Serializable;

/**
 *
 * @author Pedro Costa
 */
public class CriteriaFilterValueString implements ICriteriaFilterValue<String>, Serializable
{
    private final String value;
    
    public CriteriaFilterValueString(String value)
    {
        this.value = value;
    }
    
    @Override
    public String value()
    {
        return this.value;
    }
    
}
