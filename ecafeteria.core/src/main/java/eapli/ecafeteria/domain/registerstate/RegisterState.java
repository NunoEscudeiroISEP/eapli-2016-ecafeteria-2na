/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.domain.registerstate;

import eapli.framework.domain.AggregateRoot;
import java.io.Serializable;

/**
 *
 * @author guest
 */
public abstract class RegisterState implements Serializable {
    
    public abstract RegisterState setOpenState();
    public abstract RegisterState setCloseState();
    
}
