/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.domain.registerstate;

/**
 *
 * @author guest
 */
public class RegisterOpenState extends RegisterState{

    public RegisterOpenState() {
    }

    @Override
    public RegisterOpenState setOpenState() {
        return new RegisterOpenState();
    }

    @Override
    public RegisterOpenState setCloseState() {
        return null;
    }

    @Override
    public String toString() {
        return "RegisterOpenState{" + '}';
    }
    
    
}
