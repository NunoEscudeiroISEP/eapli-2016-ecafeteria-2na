/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.domain.registerstate;

/**
 *
 * @author guest
 */
public class RegisterCloseState extends RegisterState{

    public RegisterCloseState() {
    }

    
    @Override
    public RegisterOpenState setOpenState() {
        return null;
    }

    @Override
    public RegisterCloseState setCloseState() {
        return new RegisterCloseState();
    }

    @Override
    public String toString() {
        return "RegisterCloseState{" + '}';
    }
    
    
    
}