/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.domain.register;

import eapli.ecafeteria.domain.registerstate.RegisterCloseState;
import eapli.ecafeteria.domain.registerstate.RegisterOpenState;
import eapli.ecafeteria.domain.registerstate.RegisterState;
import eapli.framework.domain.AggregateRoot;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;


// TODO é preciso o caso de uso 'adicionar caixa'
// ficou combinado com o Professor existir apenas uma caixa

/**
 *
 * @author guest
 */
@Entity
public class Register implements AggregateRoot<String>, Serializable {
    
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue
    private Long id;

    @Column(unique = true)
    private String ref;
    private String name;
    private String description;
    private RegisterState registerState;
    private String organicUnit;
    private static Register instance = new Register();

    public Register() {
        this.registerState = new RegisterCloseState();
        instance=this;
    }

    public Register(String ref, String name, String description, String orgUnitAcronym) {
        if (ref == null || ref.trim().isEmpty() || name == null || description == null || orgUnitAcronym == null) {
            throw new IllegalArgumentException();
        }
        this.ref = ref;
        this.name = name;
        this.description = description;
        this.registerState = new RegisterCloseState();
        this.organicUnit = orgUnitAcronym;
        instance=this;
    }
    
        
    public boolean setOpenRegister(){
        registerState=new RegisterOpenState();
        return true;
    }
    
    public boolean setCloseRegister(){
        registerState=new RegisterCloseState();
        return true;
    }
    
    public String name() {
        return this.name;
    }

    public String description() {
        return this.description;
    }
    
    @Override
    public boolean sameAs(Object other) {
        return this.equals(other);
    }

    @Override
    public boolean is(String id) {
        return id.equalsIgnoreCase(this.ref);
    }

    @Override
    public String id() {
        return this.ref;
    }
    //Get the only object available
    public static Register getInstance(){
        return instance;
    }
    
    public boolean isOpen()    {
        return this.registerState instanceof RegisterOpenState;
    }
    
    
    

    @Override
    public String toString() {
        return "Register{" + "id=" + id + ", ref=" + ref + ", name=" + name + ", description=" + description + ", registerState=" + registerState.toString() + ", organicUnit=" + organicUnit + '}';
    }
    
    
    
}
