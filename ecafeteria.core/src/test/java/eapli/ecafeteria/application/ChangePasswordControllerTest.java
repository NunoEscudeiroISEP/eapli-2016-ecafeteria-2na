/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.application;

import eapli.ecafeteria.domain.authz.EmailAddress;
import eapli.ecafeteria.domain.authz.Name;
import eapli.ecafeteria.domain.authz.Password;
import eapli.ecafeteria.domain.authz.RoleSet;
import eapli.ecafeteria.domain.authz.SystemUser;
import eapli.ecafeteria.domain.authz.Username;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author guest
 */
public class ChangePasswordControllerTest {
    
    public ChangePasswordControllerTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of changePassword method, of class ChangePasswordController.
     */
    @Test
    public void testChangePassword() {
        System.out.println("changePassword");
        Password password = new Password("1234");
        ChangePasswordController instance = new ChangePasswordController();
        RoleSet roleset=new RoleSet();
        SystemUser user = new SystemUser(new Username("user1"), new Password("0000"), 
                new Name("firstname","lastname"), new EmailAddress("email@email.com"),roleset);
        user.changePassword(password);
        assertTrue(user.passwordMatches(password));
        
    }

    /**
     * Test of samePassword method, of class ChangePasswordController.
     */
    @Test
    public void testSamePassword() {
        System.out.println("samePassword");
        RoleSet roleset=new RoleSet();
        SystemUser user = new SystemUser(new Username("user1"), new Password("0000"), 
                new Name("firstname","lastname"), new EmailAddress("email@email.com"),roleset);
        Password password = new Password("0000");
        ChangePasswordController instance = new ChangePasswordController();
        assertTrue(user.passwordMatches(password));
        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
    }
    
}
