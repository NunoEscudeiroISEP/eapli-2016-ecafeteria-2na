/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.domain.register;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author guest
 */
public class RegisterTest {
    
    public RegisterTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of setOpenRegister method, of class Register.
     */
    @Test
    public void testSetOpenRegister() {
        System.out.println("setOpenRegister");
        Register instance = new Register();
        boolean expResult = true;
        boolean result = instance.setOpenRegister();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
    }

    /**
     * Test of setCloseRegister method, of class Register.
     */
    @Test
    public void testSetCloseRegister() {
        System.out.println("setCloseRegister");
        Register instance = new Register();
        boolean expResult = true;
        boolean result = instance.setCloseRegister();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
    }

    /**
     * Test of name method, of class Register.
     */
    @Test
    public void testName() {
        System.out.println("name");
        Register instance = new Register("ref","name","desc","acronym");
        String expResult = "name";
        String result = instance.name();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
    }

    /**
     * Test of description method, of class Register.
     */
    @Test
    public void testDescription() {
        System.out.println("description");
        Register instance = new Register("ref","name","desc","acronym");
        String expResult = "desc";
        String result = instance.description();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
    }

    /**
     * Test of sameAs method, of class Register.
     */
    @Test
    public void testSameAs() {
        System.out.println("sameAs");
        Object other = new Register("ref","name","desc","acronym");
        Register instance = new Register("ref","name","desc","acronym");
        boolean expResult = false;
        boolean result = instance.sameAs(other);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
    }

    /**
     * Test of is method, of class Register.
     */
    @Test
    public void testIs() {
        System.out.println("is");
        String id = "ref";
        Register instance = new Register("ref","name","desc","acronym");
        boolean expResult = true;
        boolean result = instance.is(id);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
    }

    /**
     * Test of id method, of class Register.
     */
    @Test
    public void testId() {
        System.out.println("id");
        Register instance = new Register("ref","name","desc","acronym");
        String expResult = "ref";
        String result = instance.id();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
    }

    /**
     * Test of getInstance method, of class Register.
     */
    @Test
    public void testGetInstance() {
        System.out.println("getInstance");
        Register instance = new Register("ref","name","desc","acronym");
        Register expResult = instance;
        Register result = Register.getInstance();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
    }

    /**
     * Test of isOpen method, of class Register.
     */
    @Test
    public void testIsOpen() {
        System.out.println("isOpen");
        Register instance = new Register();
        boolean expResult = false;
        boolean result = instance.isOpen();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
    }

    /**
     * Test of toString method, of class Register.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        Register instance = new Register();
        String expResult = "Register{id=null, ref=null, name=null, description=null, registerState=RegisterCloseState{}, organicUnit=null}";
        String result = instance.toString();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
    }
    
}
