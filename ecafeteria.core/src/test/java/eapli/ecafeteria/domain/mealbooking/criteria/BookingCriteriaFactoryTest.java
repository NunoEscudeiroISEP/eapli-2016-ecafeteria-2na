/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.domain.mealbooking.criteria;

import eapli.ecafeteria.domain.meals.MealType;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author André Oliveira
 */
public class BookingCriteriaFactoryTest {
    
    ICriteriaFilterValue<String> criteriaValue = new CriteriaFilterValueString(MealType.DINNER.toString());
    BookingCriteria bookingCriteria = new BookingCriteria("dishType", criteriaValue);

    public BookingCriteriaFactoryTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of generateBookingCriteria method, of class BookingCriteriaFactory.
     */
    @Test
    public void testGenerateReservationsCriteria_breakfast() {
        System.out.println("generateBookingCriteria breakfast");
        ICriteriaFactory instance = new BookingCriteriaFactory();
        ICriteria expResult = instance.generateBookingCriteria("mealType", MealType.BREAKFAST.toString());

        String criteriaName = "mealType";
        
        assertEquals(criteriaName, expResult.getCriteriaFilterName());
        assertEquals(MealType.BREAKFAST, expResult.getCriteriaFilter().value());
    }
    
    /**
     * Test of generateBookingCriteria method, of class BookingCriteriaFactory.
     */
    @Test
    public void testGenerateReservationsCriteria_lunch() {
        System.out.println("generateBookingCriteria lunch");
        ICriteriaFactory instance = new BookingCriteriaFactory();
        ICriteria expResult = instance.generateBookingCriteria("mealType", MealType.LUNCH.toString());

        String criteriaName = "mealType";
        
        assertEquals(criteriaName, expResult.getCriteriaFilterName());
        assertEquals(MealType.LUNCH, expResult.getCriteriaFilter().value());
    }
    
    /**
     * Test of generateBookingCriteria method, of class BookingCriteriaFactory.
     */
    
    /**
     * Test of generateBookingCriteria method, of class BookingCriteriaFactory.
     */
    @Test
    public void testGenerateReservationsCriteria_newcriteria() {
        System.out.println("generateBookingCriteria newcriteria");
        ICriteriaFactory instance = new BookingCriteriaFactory();
        ICriteria expResult = instance.generateBookingCriteria("dishType", MealType.DINNER.toString());

        assertEquals(this.bookingCriteria.getCriteriaFilterName(), expResult.getCriteriaFilterName());
        assertEquals(this.bookingCriteria.getCriteriaFilter().value(), expResult.getCriteriaFilter().value());
    }
    
}
