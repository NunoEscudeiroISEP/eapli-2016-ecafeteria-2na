/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.domain.mealbooking.criteria;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.*;

/**
 *
 * @author André Oliveira
 */
public class BookingCriteriaTest {
    
    ICriteriaFilterValue<String> criteriaValue = new CriteriaFilterValueString("2");
    BookingCriteria bookingCriteria = new BookingCriteria("dishType", criteriaValue);
    
    public BookingCriteriaTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getCriteriaFilter method, of class BookingCriteria.
     */
    @Test
    public void testGetCriteriaFilterValue() {
        System.out.println("getCriteriaFilter");

        ICriteriaFilterValue<String> expResult = new CriteriaFilterValueString("2");

        assertEquals(expResult.value(), this.bookingCriteria.getCriteriaFilter().value());

    }

    /**
     * Test of getCriteriaFilterName method, of class BookingCriteria.
     */
    @Test
    public void testGetCriteriaFilterName() {
        System.out.println("getCriteriaFilterName");
        String expResult = "dishType";

        assertEquals(expResult, this.bookingCriteria.getCriteriaFilterName());

    }
    
}
