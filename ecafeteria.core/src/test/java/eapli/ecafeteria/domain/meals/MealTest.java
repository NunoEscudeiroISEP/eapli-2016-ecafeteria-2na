/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.domain.meals;

import eapli.util.DateTime;
import java.util.Calendar;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author André Oliveira
 */
public class MealTest {
    
    public MealTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of numberOfConfectionMeals method, of class Meal.
     */
    @Test
    public void testNumberOfConfectionMeals() {
        System.out.println("numberOfConfectionMeals");
        int confectionMeals = 5;
        
        DishType dishType = new DishType("Peixe", "Bacalhau");
        NutritionalInfo nutricionalInfo = new NutritionalInfo(200, 20.0f, 20.0f);
        Dish dish = new Dish("Dish", dishType, nutricionalInfo, 2.5f);
        Meal instance = new Meal(dish, MealType.LUNCH, DateTime.now(), 0);
        instance.numberOfConfectionMeals(confectionMeals);

        assertTrue(instance.getNumberOfConfectionDishes() == confectionMeals);

    }

    @Test
    public void testNumberOfNotSoldMeals() {
        System.out.println("numberOfNotSoldMeals");
        int confectionMeals = 15;
        int numberOfDishes = 13;

        int SOLD = numberOfDishes-confectionMeals;

        DishType dishType = new DishType("Peixe", "Bacalhau");
        NutritionalInfo nutricionalInfo = new NutritionalInfo(200, 20.0f, 20.0f);
        Dish dish = new Dish("Dish", dishType, nutricionalInfo, 2.5f);
        Meal instance = new Meal(dish, MealType.LUNCH, DateTime.now(), 0);
        instance.numberOfConfectionMeals(confectionMeals);
        instance.changeNumberOfDishes(numberOfDishes);
        instance.numberOfNotSoldMeals();
        assertTrue(instance.getNumberOfNotSoldDishes() == SOLD);

    }

    
}
