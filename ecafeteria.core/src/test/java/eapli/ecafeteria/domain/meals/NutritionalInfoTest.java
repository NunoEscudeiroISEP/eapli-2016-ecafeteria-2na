package eapli.ecafeteria.domain.meals;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by berme on 5/19/2016.
 */
public class NutritionalInfoTest {
    private Integer validCalories = new Integer(1);
    private Float validFats = new Float(1);
    private Float validSalt = new Float(1);

    @Test(expected = IllegalArgumentException.class)
    public void testCaloriesMustNotBeNeg() {
        System.out.println("must have non-negative calories");
        new NutritionalInfo(-1, this.validFats, this.validSalt);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCaloriesMustNotBeNull() {
        System.out.println("must have calories");
        new NutritionalInfo(null, this.validFats, this.validSalt);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testFatsMustNotBeNeg() {
        System.out.println("must have non-negative fats");
        new NutritionalInfo(this.validCalories, new Float(-0.1), this.validSalt);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testFatsMustNotBeNull() {
        System.out.println("must have fats");
        new NutritionalInfo(this.validCalories, null, this.validSalt);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testSaltMustNotBeNeg() {
        System.out.println("must have non-negative salt");
        new NutritionalInfo(this.validCalories, this.validFats, new Float(-0.1));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testSaltMustNotBeNull() {
        System.out.println("must have salt");
        new NutritionalInfo(this.validCalories, this.validFats, null);
    }

    public void testNutrionalInfoValid() {
        System.out.println("must have salt");
        try {
            new NutritionalInfo(this.validCalories, this.validFats, this.validSalt);
        } catch (IllegalArgumentException e) {
            fail();
        }
    }

}