/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.domain.meals;

import eapli.util.DateTime;
import java.util.*;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author soliveira
 */
public class MenuTest {
    
    DishType dishType1;
    DishType dishType2;
    
    NutritionalInfo nutricionalInfo1;
    
    Dish dish1;
    Dish dish2;
    Dish dish3;
    Dish dish4;
    Dish dish5;
    
    Meal meal1;
    Meal meal2;
    Meal meal3;
    Meal meal4;
    Meal meal5;
    
    Set<Meal> listMeal;
    
    Period period;
    
    Menu menu1;
        
    public MenuTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        
        this.dishType1 = new DishType("Peixe", "Prato de peixe");
        this.dishType2 = new DishType("Carne", "Prato de carne");
        
        this.nutricionalInfo1 = new NutritionalInfo(200, 20.0f, 20.0f);
        
        this.dish1 = new Dish("Bacalhau Assado", this.dishType1, nutricionalInfo1, 2.5f);
        this.dish2 = new Dish("Rã Assada", this.dishType1, nutricionalInfo1, 2.5f);
        this.dish3 = new Dish("PeixeEspada Assado", this.dishType1, nutricionalInfo1, 2.5f);
        this.dish4 = new Dish("Carne Assada", this.dishType2, nutricionalInfo1, 2.5f);
        this.dish5 = new Dish("Feveras Fritas", this.dishType2, nutricionalInfo1, 2.5f);
        
        this.meal1 = new Meal(this.dish1, MealType.LUNCH, DateTime.now(), 5);
        this.meal2 = new Meal(this.dish1, MealType.LUNCH, DateTime.now(), 5);
        this.meal3 = new Meal(this.dish1, MealType.LUNCH, DateTime.now(), 5);
        this.meal4 = new Meal(this.dish4, MealType.LUNCH, DateTime.now(), 5);
        this.meal5 = new Meal(this.dish5, MealType.LUNCH, DateTime.now(), 5);
        
        this.listMeal = new HashSet<>();
        
        this.listMeal.add(meal1);
        this.listMeal.add(meal2);
        this.listMeal.add(meal3);
        this.listMeal.add(meal4);
        this.listMeal.add(meal5);
        
        this.period = new Period(DateTime.now(), DateTime.now());
        
        this.menu1 = new Menu(this.period, this.listMeal);
        
    }
    
    @After
    public void tearDown() {
    }

    @Test(expected = IllegalArgumentException.class)
    public void testPeriodMustNotBeNull() {
        System.out.println("menu must have a period");
        new Menu(null, new HashSet<Meal>());
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void testMealsMustNotNull() {
        System.out.println("menu must have a set of meals");
        
        Period p = new Period(1);
        
        new Menu(p, null);
    }
    
    @Test
    public void testDefineNewPeriod() {
        System.out.println("Menu -> Test Define New Period");
        
        Period p1 = new Period(1);
        Period newPeriod = new Period(2);
        
        Set<Meal> meals = new HashSet<>();
        Menu theMenu = new Menu(p1, meals);
        
        theMenu.defineNewPeriod(newPeriod);
        
         assertEquals(newPeriod, theMenu.period());
    }

    /**
     * Test of mealOfType method, of class Menu.
     */
    @Test
    public void testMealOfType() {
        System.out.println("mealOfType");
        
        List<Meal> expResult = new ArrayList<>();
        
        expResult.add(meal1);
        expResult.add(meal2);
        expResult.add(meal3);
        
        List<Meal> result = this.menu1.mealOfType(this.dishType1);
        
        
        System.out.println(result.get(0).dishType().description());
        System.out.println(expResult.get(0).dishType().description());
        
        assertTrue(result.get(0).sameDishType(expResult.get(0).dishType().acronym()));
        assertTrue(result.get(1).sameDishType(expResult.get(1).dishType().acronym()));
        assertTrue(result.get(2).sameDishType(expResult.get(2).dishType().acronym()));

    }

}
