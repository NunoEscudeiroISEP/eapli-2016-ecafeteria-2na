package eapli.ecafeteria.domain.meals;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.*;

/**
 * Created by berme on 5/19/2016.
 */
public class DishTest
{
    private NutritionalInfo validInfo;

    private DishType validType;

    private Float price;

    private String name;

    @Before
    public void setUp() {
        this.validInfo = new NutritionalInfo(new Integer(13), new Float(1.5), new Float(1.2));
        this.validType = new DishType("microbiotic", "low qtd, high protein");
        this.name = new String("Test Dish");
        this.price = new Float(1.5);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testNameMustNotBeEmpty() {
        System.out.println("must have non-empty name");
        new Dish("", this.validType, this.validInfo, this.price);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testAcronymMustNotBeNull() {
        System.out.println("must have an name");
        new Dish(null, this.validType, this.validInfo, this.price);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testDishTypeMustNotBeNull() {
        System.out.println("must have a dish type");
        new Dish(this.name, null, this.validInfo, this.price);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testNutritionalInfoMustNotBeNull() {
        System.out.println("must have the nutritional information");
        new Dish(this.name, this.validType, null, this.price);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testPriceInfoMustNotBeNegative() {
        System.out.println("must have a non-negative price");
        new Dish(this.name, this.validType, this.validInfo, new Float(-0.1));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testPriceMustNotBeNull() {
        System.out.println("must have a price");
        new Dish(this.name, this.validType, this.validInfo, null);
    }

    public void testValidDish() {
        System.out.println("successfull creation");
        try {
            new Dish(this.name, this.validType, this.validInfo, this.price);
        } catch (IllegalArgumentException e) {
            fail();
        }
    }

    /**
     * Test of is method, of class Dish.
     */
    @Test
    public void testIs() {
        System.out.println("test is method");
        final Dish instance = new Dish(this.name, this.validType, this.validInfo, this.price);
        assertTrue(instance.is(this.name));
    }

    /**
     * Test of is method, of class Dish.
     */
    @Test
    public void testIsNot() {
        System.out.println("must have same id");
        final Dish instance = new Dish(this.name, this.validType, this.validInfo, this.price);
        assertFalse(instance.is("A"));
    }

    /**
     * Test of id method, of class Dish.
     */
    @Test
    public void testId() {
        System.out.println("test id");
        final Dish instance = new Dish(this.name, this.validType, this.validInfo, this.price);
        assertTrue(this.name.equals(instance.id()));
    }

    /**
     * Test of sameType method, of class Dish.
     */
    @Test
    public void testSameType() {
        System.out.println("test sameType method");
        final Dish instance = new Dish(this.name, this.validType, this.validInfo, this.price);
        assertTrue(instance.sameType(this.validType.id()));
    }

    /**
     * Test of sameType method, of class Dish.
     */
    @Test
    public void testSameTypeNot() {
        System.out.println("test sameType method");
        final Dish instance = new Dish(this.name, this.validType, this.validInfo, this.price);
        assertFalse(instance.sameType("A"));
    }

    /**
     * Test of sameName method, of class Dish.
     */
    @Test
    public void testSameName() {
        System.out.println("test sameName method");
        final Dish instance = new Dish(this.name, this.validType, this.validInfo, this.price);
        assertTrue(instance.sameName(this.name));
    }

    /**
     * Test of sameName method, of class Dish.
     */
    @Test
    public void testSameNameNot() {
        System.out.println("test sameName method");
        final Dish instance = new Dish(this.name, this.validType, this.validInfo, this.price);
        assertFalse(instance.sameName("A"));
    }

    /**
     * Test of sameAs method, of class Dish.
     */
    @Test
    public void testSameAsRefence() {
        System.out.println("test sameName method");
        final Dish instance = new Dish(this.name, this.validType, this.validInfo, this.price);
        assertTrue(instance.sameAs(instance));
    }

    /**
     * Test of sameAs method, of class Dish.
     */
    @Test
    public void testSameAs() {
        System.out.println("test sameName method");
        final Dish instance = new Dish(this.name, this.validType, this.validInfo, this.price);
        assertTrue(instance.sameAs(new Dish(this.name, this.validType, this.validInfo, this.price)));
    }

    /**
     * Test of sameAs method, of class Dish.
     */
    @Test
    public void testSameAsNot() {
        System.out.println("test sameName method");
        final Dish instance = new Dish("A", this.validType, this.validInfo, this.price);
        assertFalse(instance.sameAs(new Dish(this.name, this.validType, this.validInfo, this.price)));
    }
}