/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.utente.consoleapp.presentation.meals;

import eapli.ecafeteria.AppSettings;
import eapli.ecafeteria.application.RegisterBookingController;
import eapli.ecafeteria.domain.authz.SystemUser;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.domain.meals.Menu;
import eapli.ecafeteria.persistence.CafeteriaUserRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.framework.application.Controller;
import eapli.util.Console;
import eapli.framework.persistence.DataIntegrityViolationException;
import eapli.framework.presentation.console.AbstractUI;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 *
 * @author Sara Ramos
 */
public class RegisterBookingUI extends AbstractUI
{
    private final RegisterBookingController controller = new RegisterBookingController();

    protected Controller controller() {
        return this.controller;
    }

    @Override
    protected boolean doShow() {

        List<Meal> currentMeals = new ArrayList<>();
              
        StringBuilder stringBuilder = new StringBuilder("Select from available meals:\n");
        Integer option=1;
        //show current menu
        
        Menu menu = this.controller.showCurrentMenu();
        
        if(menu == null){
            System.out.println("No available menus");
            return false;
        }
        Set<Meal> setMeals = menu.meals();
        for (Meal meal : setMeals) {
            currentMeals.add(meal);
            stringBuilder.append(option);
            stringBuilder.append(" - ");
            stringBuilder.append(meal.formatedMealDate());
            stringBuilder.append(" - ");
            stringBuilder.append(meal.mealType());
            stringBuilder.append("\n");
            stringBuilder.append(meal.dishType());
            stringBuilder.append("\n");
            stringBuilder.append(meal.dishName());
            stringBuilder.append("\n");
            option++;
        }
        
        stringBuilder.append("\nOption:");
        final String userChoice = Console.readLine(stringBuilder.toString());
        // selected used meal
        Meal selectedMeal = currentMeals.get(Integer.parseInt(userChoice)-1);
              
        //Register booking
        try {
            this.controller.registerBooking(selectedMeal);
        } catch (DataIntegrityViolationException ex) {
            System.out.println("This booking is already defined.");
        }
            return false;
        }

    @Override
    public String headline() {
        return "Register Booking";
    }
}
