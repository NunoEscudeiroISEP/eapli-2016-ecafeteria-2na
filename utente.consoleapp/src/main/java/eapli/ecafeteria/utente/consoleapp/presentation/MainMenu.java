/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates and open the template
 * in the editor.
 */
package eapli.ecafeteria.utente.consoleapp.presentation;

import eapli.cafeteria.consoleapp.presentation.ExitWithMessageAction;
import eapli.cafeteria.consoleapp.presentation.MyUserMenu;
import eapli.ecafeteria.AppSettings;
import eapli.ecafeteria.application.BalanceController;
import eapli.ecafeteria.application.ReturnCafeteriaUserWidthSystemUserController;
import eapli.ecafeteria.domain.authz.ActionRight;
import eapli.ecafeteria.domain.mealbooking.CafeteriaUser;
import eapli.framework.persistence.DataIntegrityViolationException;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.presentation.console.Menu;
import eapli.framework.presentation.console.MenuItem;
import eapli.framework.presentation.console.MenuRenderer;
import eapli.framework.presentation.console.ShowVerticalSubMenuAction;
import eapli.framework.presentation.console.SubMenu;
import eapli.framework.presentation.console.VerticalMenuRenderer;
import eapli.framework.presentation.console.VerticalSeparator;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Paulo Gandra Sousa
 */
public class MainMenu extends AbstractUI {

    private static final int EXIT_OPTION = 0;

    // MAIN MENU
    private static final int MY_USER_OPTION = 1;
    private static final int BOOKINGS_OPTION = 2;

    /*
     * Option not being used private static final int DISH_TYPE_OPTION = 2;
     */
    public MainMenu() {
    }

    @Override
    public boolean show() {
        drawFormTitle();
        return doShow();
    }

    /**
     * @return true if the user selected the exit option
     */
    @Override
    public boolean doShow() {
        final Menu menu = buildMainMenu();
        final MenuRenderer renderer = new VerticalMenuRenderer(menu);
        return renderer.show();
    }

    @Override
    public String headline() {
        ReturnCafeteriaUserWidthSystemUserController cafUserControler = new ReturnCafeteriaUserWidthSystemUserController();
        CafeteriaUser cafUser = cafUserControler.returnCafeteriaUser(AppSettings.instance().session().authenticatedUser());
            
            if(cafUser != null){
                BalanceController bc = new BalanceController();
                double balance = 0;
                try {
                    balance = bc.getBalance(cafUser.account());
                } catch (DataIntegrityViolationException ex) {
                    Logger.getLogger(MainMenu.class.getName()).log(Level.SEVERE, null, ex);
                }
                return "eCAFETERIA [@" + AppSettings.instance().session().authenticatedUser().id() + " - Balance ;"+ balance +"€]";
            }else{
                return "eCAFETERIA [@" + AppSettings.instance().session().authenticatedUser().id() + "]";
            }
    }

    private Menu buildMainMenu() {
        final Menu mainMenu = new Menu();

        final Menu myUserMenu = new MyUserMenu();
        mainMenu.add(new SubMenu(MY_USER_OPTION, myUserMenu, new ShowVerticalSubMenuAction(myUserMenu)));

        mainMenu.add(VerticalSeparator.separator());

        if(AppSettings.instance().session().authenticatedUser().isAuthorizedTo(ActionRight.CafeteriaUser))
        {
            final Menu bookingsMenu = new BookingsMenu();
            mainMenu.add(new SubMenu(BOOKINGS_OPTION, bookingsMenu, new ShowVerticalSubMenuAction(bookingsMenu)));
        }

        // TODO add menu options

        mainMenu.add(VerticalSeparator.separator());

        mainMenu.add(new MenuItem(EXIT_OPTION, "Exit", new ExitWithMessageAction()));

        return mainMenu;
    }
}
