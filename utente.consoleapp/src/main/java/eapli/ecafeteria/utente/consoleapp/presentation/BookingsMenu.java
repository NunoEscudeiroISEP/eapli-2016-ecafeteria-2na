/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.utente.consoleapp.presentation;

import eapli.ecafeteria.utente.consoleapp.presentation.meals.RegisterBookingAction;
import eapli.framework.actions.ReturnAction;
import eapli.framework.presentation.console.Menu;
import eapli.framework.presentation.console.MenuItem;

/**
 *
 * @author Sara Ramos
 */
public class BookingsMenu extends Menu
{
    private static final int EXIT_OPTION = 0;
    private static final int REGISTER_BOOKING = 1;

    public BookingsMenu() {
        super("My Bookings >");
        buildBookingsMenu();
    }

    private void buildBookingsMenu() {
        add(new MenuItem(REGISTER_BOOKING, "Register Booking ", new RegisterBookingAction()));
        add(new MenuItem(EXIT_OPTION, "Return ", new ReturnAction()));
    }
    
}
