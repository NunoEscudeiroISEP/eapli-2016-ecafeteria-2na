/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.cafeteria.consoleapp.presentation.authz;

import eapli.ecafeteria.application.ChangePasswordController;
import eapli.ecafeteria.domain.authz.Password;
import eapli.framework.application.Controller;
import eapli.framework.presentation.console.AbstractUI;
import eapli.util.Console;

/**
 *
 * @author guest
 */
public class ChangePasswordUI extends AbstractUI{
    private final ChangePasswordController theController = new ChangePasswordController();

    /*
     * private final Logger logger = LoggerFactory.getLogger(LoginUI.class);
     * 
     * public Logger getLogger() { return logger; }
     */

    protected Controller controller() {
        return this.theController;
    }

    @Override
    protected boolean doShow() {
        final String oldPassword = Console.readLine("Old password:");
        Password oldPassword1=new Password(oldPassword);
        if(!this.theController.samePassword(oldPassword1)){
            System.out.println("Old password does not match");
            return false;
        }
        final String newPasswordA = Console.readLine("New password:");
        Password newPassword1=new Password(newPasswordA);
        final String newPasswordB = Console.readLine("Retype new Password:");
        Password newPassword2=new Password(newPasswordB);
        if(!newPassword1.equals(newPassword2)){
            System.out.println("New password does not match");
            return false;
        }
        if(this.theController.changePassword(newPassword1)){
            System.out.println("Strength is "+newPassword1.strength().toString()+"\n"+"Successful password change.");
            return true;
        }else{
            System.out.println("Strength is "+newPassword1.strength().toString()+"\n"+"Unsuccessful password change.");
            return false;
        }
    }

    @Override
    public String headline() {
        return "Login";
    }
}
