/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.bootstrapapp;

import eapli.ecafeteria.application.AddCafeteriaUserController;
import eapli.ecafeteria.domain.authz.RoleType;
import eapli.framework.actions.Action;
import eapli.util.DateTime;
import java.util.Calendar;
import java.util.HashSet;
import java.util.Set;

public class CafeteriaUserBootstrap implements Action {
    
    @Override
    public boolean execute() {
        registerCafeteriaUser();
        return false;
    }

    /**
     *
     */
    private void registerCafeteriaUser() {
        final String username = "utente";
        final String password = "utente";

        final String firstName = "JohnUtente";
        final String lastName = "doe";

        final String email = "johnutente.doe@emai.l.com";

        final Set<RoleType> roles = new HashSet<RoleType>();
        roles.add(RoleType.Utente);
        
        Calendar createdOn;
        
        String mecanographicNumber = "1120035";
        
        String acronym = "HIMYM";
        String name = "How I Met Your Mother";
        String description = "Yo mamma so fat";
        String account = "112233";

        final AddCafeteriaUserController userCafeteriaController = new AddCafeteriaUserController();
        try {
            userCafeteriaController.addCafeteriaUser(username, password, firstName, lastName, email, roles, DateTime.now(), mecanographicNumber, acronym, name, description, account);
        } catch (final Exception e) {
            System.out.println("CAFETARI UASER EXCEPTION" + e.toString());
        }
    }
}
