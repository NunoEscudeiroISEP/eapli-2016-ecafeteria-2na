/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.bootstrapapp;

import eapli.ecafeteria.domain.meals.Dish;
import eapli.ecafeteria.domain.meals.MealBuilder;
import eapli.ecafeteria.domain.meals.MealType;
import eapli.ecafeteria.domain.meals.MenuBuilder;
import eapli.ecafeteria.domain.meals.Period;
import eapli.ecafeteria.persistence.DishRepository;
import eapli.ecafeteria.persistence.MenuRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.framework.actions.Action;
import eapli.framework.persistence.DataIntegrityViolationException;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author soliveira
 */
public class MenuBootstrap implements Action {

    @Override
    public boolean execute() {

        int startWeek = 1;
        int endWeek = 25;
        
        for(int i = startWeek; i<= endWeek; i++){
            try {
                register(i);
            } catch (DataIntegrityViolationException ex) {
                Logger.getLogger(MenuBootstrap.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        return false;
    }

    private void register(int weekNumber) throws DataIntegrityViolationException {
        DishRepository dishRepo = PersistenceContext.repositories().dishes();
        
        Period period = new Period(weekNumber);
        MealBuilder mealBuilder = new MealBuilder(period);
        MenuBuilder menuBuilder = new MenuBuilder();
        
        Iterable<Dish> dishes = dishRepo.all(); //assume-se que existem pratos ja criados no DishBootstrap
        
        Dish dish = dishes.iterator().next();
        menuBuilder.referringToWeek(weekNumber);

        for (int day = 1; day <= 7; day++) {
            mealBuilder.forWeekDay(day).forMealType(MealType.BREAKFAST).withDish(dish).withNumberOfDishes(10);

            menuBuilder.withMeal(mealBuilder.build());
            
            mealBuilder.forWeekDay(day).forMealType(MealType.LUNCH).withDish(dish).withNumberOfDishes(10);

            menuBuilder.withMeal(mealBuilder.build());
            
            mealBuilder.forWeekDay(day).forMealType(MealType.DINNER).withDish(dish).withNumberOfDishes(10);

            menuBuilder.withMeal(mealBuilder.build());
        }
        
        MenuRepository menuRepo = PersistenceContext.repositories().menus();
        menuRepo.add(menuBuilder.build());
    }
}
