/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.bootstrapapp;

import eapli.ecafeteria.application.ChangeDishTypeController;
import eapli.ecafeteria.application.RegisterDishController;
import eapli.ecafeteria.domain.meals.DishType;
import eapli.ecafeteria.domain.meals.NutritionalInfo;
import eapli.framework.actions.Action;

/**
 *
 * @author soliveira
 */
public class DishBootstrap implements Action{

    @Override
    public boolean execute() {
        register();
        
        return false;
    }
    
    private void register() {
        final RegisterDishController registerController = new RegisterDishController();
        final String dishName = "Prato ";
        int counter = 1;
        
        try {
            Iterable<DishType> list = registerController.listActiveDishTypes();
            for(DishType t:list){
                registerController.registerDish(dishName + counter, t, new NutritionalInfo(1, 1f, 1f), 1f);     
                counter++;
            }
            
        } catch (final Exception e) {
            // ignoring exception. assuming it is just a primary key violation
            // due to the tentative of inserting a duplicated user
        }
    }
    
}
