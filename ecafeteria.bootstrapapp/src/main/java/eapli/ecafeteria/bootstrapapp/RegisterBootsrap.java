/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.bootstrapapp;

import eapli.ecafeteria.application.AddOrganicUnitController;
import eapli.ecafeteria.application.AddRegisterController;
import eapli.framework.actions.Action;

/**
 *
 * @author guest
 */
public class RegisterBootsrap implements Action {

    @Override
    public boolean execute() {
        register("Register", "Register", "Register", "all");
        
        return false;
    }

    /**
     *
     */
    private void register(String ref, String desc, String name, String orgU) {
        final AddRegisterController controller = new AddRegisterController();
        try {
            controller.addRegister(ref, desc, name, orgU);
        } catch (final Exception e) {
            // ignoring exception. assuming it is just a primary key violation
            // due to the tentative of inserting a duplicated organic unit
        }
    }
}
