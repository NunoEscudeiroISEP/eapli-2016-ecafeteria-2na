/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.backoffice.consoleapp.presentation.meals;

import eapli.framework.visitor.Visitor;

/**
 *
 * @author soliveira
 */
public class WeekDayPrinter implements Visitor<WeekDay>{
    
    @Override
    public void visit(WeekDay visitee) {
        System.out.println(visitee);
    }

    @Override
    public void beforeVisiting(WeekDay visitee) {
        
    }

    @Override
    public void afterVisiting(WeekDay visitee) {
        
    }
}
