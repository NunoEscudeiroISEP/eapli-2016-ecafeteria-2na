/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.backoffice.consoleapp.presentation.meals;

import eapli.ecafeteria.application.RegistrationOfConfectionMealsController;
import eapli.ecafeteria.domain.meals.DishType;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.framework.application.Controller;
import eapli.framework.presentation.console.AbstractUI;
import eapli.util.Console;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 *
 * @author André Oliveira
 */
public class RegistrationOfConfectionMealsUI extends AbstractUI{
    
    //seleciona o menu atual logo que é instânciado o controller
    private final RegistrationOfConfectionMealsController theController = 
                new RegistrationOfConfectionMealsController();
    
    protected Controller controller() {
        return this.theController;
    }

    @Override
    protected boolean doShow() {

        if(!theController.findMenuByDate())
        {
            System.out.print("Não existem menus para a data atual");
            return false;
        }

     
        Iterable<DishType> dishTypeList = theController.listAllMealsTypes();
        StringBuilder options = new StringBuilder();
        
        Iterator<DishType> iterator = dishTypeList.iterator();
        List<DishType> dishTypeListTemp = new ArrayList<>();
        
        int index = 1;
        
        while(iterator.hasNext()){

            options.append(index);
            options.append(".");

            DishType dishType = iterator.next();
            options.append(dishType.description());
            dishTypeListTemp.add(dishType);

            options.append("\n");
            
            index++;
        }
        
        System.out.println(options.toString());
        
        final int option = Console.readOption(1, index-1, 0);
        
        List<Meal> listAtualMeals = this.theController.mealsOfType(dishTypeListTemp.get(option-1));
        
        for (Meal meal : listAtualMeals) {
            
            Integer numberOfMeals = Console.readInteger("Introduzir número de pratos efetivamente "
                    + "confecionados para "+ meal.dishName());

            if(theController.registrationOfConfectionMeals(numberOfMeals, meal) == null){
                return false;
            }else{
                
                System.out.println("Registo efetuado com sucesso para: "+meal.dishName());
                
            }   
        }
        
        return true;   
    }

    @Override
    public String headline() {
        return "Registration of Confection Meals";
    }
    
}
