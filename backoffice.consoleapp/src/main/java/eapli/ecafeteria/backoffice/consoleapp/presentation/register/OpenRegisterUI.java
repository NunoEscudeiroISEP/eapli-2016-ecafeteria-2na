/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.backoffice.consoleapp.presentation.register;

import eapli.ecafeteria.application.OpenRegisterController;
import eapli.framework.application.Controller;
import eapli.framework.presentation.console.AbstractUI;
import eapli.util.Console;
import eapli.util.DateTime;
import java.util.Calendar;

/**
 *
 * @author guest
 */
public class OpenRegisterUI extends AbstractUI {

    private final OpenRegisterController theController = new OpenRegisterController();

    protected Controller controller() {
        return this.theController;
    }

    @Override
    protected boolean doShow() {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        Calendar nowIs = DateTime.now();
        System.out.println("Now is " + nowIs.getTime());
        Calendar lunch = DateTime.now();
        lunch.set(Calendar.HOUR_OF_DAY, 15);
        if (nowIs.getTime().before(lunch.getTime())) {
            System.out.println("Meal: Lunch");
        } else {
            System.out.println("Meal: Dinner");
        }
        String readLine = Console.readLine("Accept this displayed Time and Meal? (Yes/no)");

        if (!(readLine.equalsIgnoreCase("") || readLine.equalsIgnoreCase("yes"))) {
            //não é feito o tratamento pois esta informação não é armazenada ou usada
            String readLineDate = Console.readLine("Insert date [yyyy-mm-dd]: ");
            String readLinetime = Console.readLine("Insert time [hh:mm:ss]: ");
            String readLineMeal = Console.readLine("Insert meal [Lunch/Dinner]: ");
        }

        if (this.theController.isOpen()) {
            System.out.println("Register already opened");
            return true;
        } else if (this.theController.openRegister()) {
            System.out.println("Register is now OPEN.");
            this.theController.saveRegister();
            return true;

        } else {
            System.out.println("ERROR while opening Register.");
        }

        return true;
    }

    @Override
    public String headline() {
        return "Open Register";
    }
}
