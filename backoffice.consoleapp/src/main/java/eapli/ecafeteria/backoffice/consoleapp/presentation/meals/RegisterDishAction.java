package eapli.ecafeteria.backoffice.consoleapp.presentation.meals;

import eapli.framework.actions.Action;

/**
 * Created by berme on 4/22/2016.
 */
public class RegisterDishAction implements Action {
    @Override
    public boolean execute() {
        return new DefineDishUI().show();
    }
}
