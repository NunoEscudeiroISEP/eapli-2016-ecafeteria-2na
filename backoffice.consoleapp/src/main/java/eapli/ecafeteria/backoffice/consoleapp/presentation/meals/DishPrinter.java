/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.backoffice.consoleapp.presentation.meals;

import eapli.ecafeteria.domain.meals.Dish;
import eapli.framework.visitor.Visitor;

/**
 *
 * @author soliveira
 */
public class DishPrinter implements Visitor<Dish> {

    @Override
    public void visit(Dish visitee) {
        System.out.printf("%-10s%-4s\n", visitee.id(), visitee.name());
    }

    @Override
    public void beforeVisiting(Dish visitee) {
        
    }

    @Override
    public void afterVisiting(Dish visitee) {
        
    }
    
}
