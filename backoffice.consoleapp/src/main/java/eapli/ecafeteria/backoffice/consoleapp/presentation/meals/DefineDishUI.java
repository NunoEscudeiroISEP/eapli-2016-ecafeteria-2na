package eapli.ecafeteria.backoffice.consoleapp.presentation.meals;

import eapli.ecafeteria.application.RegisterDishController;
import eapli.ecafeteria.domain.meals.DishType;
import eapli.ecafeteria.domain.meals.NutritionalInfo;
import eapli.framework.application.Controller;
import eapli.framework.persistence.DataIntegrityViolationException;
import eapli.framework.presentation.console.AbstractUI;
import eapli.util.Console;
import java.util.ArrayList;
import java.util.List;
    
/**
 * Created by berme on 4/22/2016.
 */
public class DefineDishUI extends AbstractUI {
    private final RegisterDishController controller = new RegisterDishController();

    protected Controller controller() {
        return this.controller;
    }

    @Override
    protected boolean doShow() {
        final String dishName = Console.readLine("Dish name:");
        Integer option=1;
        Iterable<DishType> listActiveDishTypes = this.controller.listActiveDishTypes();
        List<DishType> activeDishTypes = new ArrayList<>();
        
        //Show all active dish types
        StringBuilder stringBuilder = new StringBuilder("Select dish type:\n");
        
        for(DishType dishType : listActiveDishTypes){
           activeDishTypes.add(dishType);
           stringBuilder.append(option);
           stringBuilder.append(" - ");
           stringBuilder.append(dishType.description());
           stringBuilder.append("\n");
           option++;
        }
        
        stringBuilder.append("\nOption:");
        final String userChoice = Console.readLine(stringBuilder.toString());
        
        //User dish type selected
        DishType dishType = activeDishTypes.get(Integer.parseInt(userChoice)-1);

        //Insert nutritional information
        System.out.println("Insert nutritional information\n ");
        final String calories = Console.readLine("Insert calories in Kcal:");
        final String salt = Console.readLine("Insert salt in grams:");
        final String fats = Console.readLine("Insert fats in grams:");
                
        NutritionalInfo nutricionalInfo = new NutritionalInfo(Integer.parseInt(calories), Float.parseFloat(salt), Float.parseFloat(fats));
        
        //Insert price
        final String price = Console.readLine("Insert price:");
                
        //Register Dish
        try {
            this.controller.registerDish(dishName, dishType, nutricionalInfo, Float.parseFloat(price));
        } catch (DataIntegrityViolationException ex) {
            System.out.println("This dish is already defined.");
        }
        
        return false;
    }

    @Override
    public String headline() {
        return "Define Dish";
    }
}
