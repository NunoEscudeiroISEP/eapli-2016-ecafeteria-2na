/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.backoffice.consoleapp.presentation.meals;

import eapli.ecafeteria.application.GenerateMenuController;
import eapli.ecafeteria.domain.meals.Dish;
import eapli.ecafeteria.domain.meals.DishType;
import eapli.ecafeteria.domain.meals.MealType;
import eapli.framework.application.*;
import eapli.framework.persistence.DataIntegrityViolationException;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.presentation.console.SelectWidget;
import eapli.util.Console;
import eapli.util.Strings;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author soliveira
 */
public class GenerateMenuUI extends AbstractUI {

    GenerateMenuController theController;

    public GenerateMenuUI() {
        theController = new GenerateMenuController();
    }

    protected Controller controller() {
        return this.theController;
    }

    @Override
    protected boolean doShow() {
        boolean isToContinueSelectingDays = true;

        //TODO @soliveira confirmar se a semana escolhida já passou.
        weekOfYear();

        while (isToContinueSelectingDays) {
            
            if(weekDay() <= 0){
                isToContinueSelectingDays = false;
                break;
            }

            String dishName = dish();
            if(Strings.isNullOrEmpty(dishName)){
                isToContinueSelectingDays = false;
                break;
            }

            if(mealType() <= 0){
                isToContinueSelectingDays = false;
                break;
            }

            numberOfDishes(dishName);

            System.out.println("Continue with selection?");
            if(!userContinueDecision()){
                isToContinueSelectingDays = false;
            }

        }

        System.out.println("Save?");
        if(userContinueDecision()){
            try {
                this.theController.save();
            } catch (DataIntegrityViolationException ex) {
                Logger.getLogger(GenerateMenuUI.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        return false;
    }

    @Override
    public String headline() {
        return "Generate a Menu";
    }

    private void weekOfYear() {
        final String weekNumber = Console.readLine("Select week number:");

        this.theController.generatePeriod(Integer.parseInt(weekNumber)); //TODO: Verificar a possivel exceção
    }

    private int weekDay() {
        WeekDayAdapter weekDayAdapter = new WeekDayAdapter();
        final SelectWidget<WeekDay> weekDaySelector = new SelectWidget<>(weekDayAdapter, new WeekDayPrinter());

        System.out.println("Select week day:");
        weekDaySelector.show();

        this.theController.defineWeekDay(weekDaySelector.selectedOption());
        
        return weekDaySelector.selectedOption();
    }

    private String dish() {
        System.out.println("Select a dish type:");
        final Iterable<DishType> dishTypes = this.theController.listDishTypes();
        final SelectWidget<DishType> dishTypeSelector = new SelectWidget<>(dishTypes, new DishTypePrinter());
        dishTypeSelector.show();

        if(dishTypeSelector.selectedOption() <= 0){
            return "";
        }
        System.out.println("Select a dish:");
        final Iterable<Dish> dishes = this.theController.AllDishes(dishTypeSelector.selectedElement());
        final SelectWidget<Dish> dishSelector = new SelectWidget<>(dishes, new DishPrinter());
        dishSelector.show();

        this.theController.defineDish(dishSelector.selectedElement());

        return dishSelector.selectedElement().name();
    }

    private int mealType() {

        System.out.println("Select a meal type:");
        MealTypeAdapter mealTypeAdapter = new MealTypeAdapter();

        final SelectWidget<MealType> mealTypeSelector = new SelectWidget<>(mealTypeAdapter, new MealTypePrinter());
        mealTypeSelector.show();

        this.theController.defineMealType(mealTypeSelector.selectedElement());
        
        return mealTypeSelector.selectedOption();
    }

    private void numberOfDishes(String dishName) {
        boolean isToContinue = true;

        while (isToContinue) {
            isToContinue = false;

            final String strNumberOfDishes = Console.readLine("Number of dishes for " + dishName + ":");

            try {
                int numberOfDishes = Integer.parseInt(strNumberOfDishes);

                this.theController.defineNumberOfDishes(numberOfDishes);
            } catch (NumberFormatException e) {
                isToContinue = true;
            }
        }
    }
    
    private boolean userContinueDecision(){
        return Console.readBoolean("(Y/N)");
    }
}
