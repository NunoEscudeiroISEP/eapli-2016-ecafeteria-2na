/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.backoffice.consoleapp.presentation.register;

import eapli.ecafeteria.application.OpenRegisterController;
import eapli.ecafeteria.application.TopUpCardController;
import eapli.framework.application.Controller;
import eapli.framework.presentation.console.AbstractUI;
import eapli.util.Console;
import java.util.NoSuchElementException;
/**
 *
 * @author 1111407
 */
public class TopUpCardUI extends AbstractUI {
    private final TopUpCardController topUpCardController = new TopUpCardController();

    protected Controller controller() {
        return this.topUpCardController;
    }

    @Override
    protected boolean doShow() {
        try {            
            OpenRegisterController registerController = new OpenRegisterController();
            
            //Check if register is open
            if(!registerController.isOpen()) {
                System.out.println("There are no Register opened! To top up a card you got to open one first.");
                return true;
            } else {
                // fetch user mec number
                String mechanographic_number = Console.readLine("Insert user mechanographic number!");
                
                // if account not created ask if want to create new one
                if(!this.topUpCardController.verifyAccount(mechanographic_number)){
                    String answer = Console.readLine("There's no account for this user, do you want to create one? (Y or N).");                    
                    if ("Y".equals(answer)) {
                        topUpCardController.createNewAccount(mechanographic_number);
                        
                    } else {
                        System.out.println("Exiting the menu.");
                        return true;
                    }                    
                }
                
                // ask for the amount to deposit
                double value = Console.readDouble("How much money do you want to deposit on your card?");
                
                
            }            
        }catch(IllegalStateException ise){
            System.out.println(ise.getMessage());
        }catch(NoSuchElementException nsee){
            System.out.println(nsee.getMessage());
        }
        
        return false;
    }

    @Override
    public String headline() {
        return "Top Up Card";
    }   
}
