/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.backoffice.consoleapp.presentation.meals;

/**
 *
 * @author soliveira
 */
public enum WeekDay {
    SUNDAY,
    MONDAY,
    TUESDAY,
    WEDNESDAY,
    THURSDAY,
    FRIDAY,
    SATURDAY;

    /**
     * Convert a string to respective enum.
     *
     * @param type String associated with enum
     *
     * @return Enum associated or null if not found.
     */
    public static WeekDay weekDayFromString(String type) {
        type = type.toLowerCase().trim();

        switch (type) {
            case "sunday":
                return SUNDAY;
            case "monday":
                return MONDAY;
            case "tuesday":
                return TUESDAY;
            case "wednesday":
                return WEDNESDAY;
            case "thursday":
                return THURSDAY;
            case "friday":
                return FRIDAY;
            case "saturday":
                return SATURDAY;
        }
        return null;
    }
}
