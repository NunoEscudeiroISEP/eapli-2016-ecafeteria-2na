/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.backoffice.consoleapp.presentation.meals;

import eapli.ecafeteria.domain.meals.Menu;
import eapli.framework.visitor.Visitor;

/**
 *
 * @author soliveira
 */
public class MenuPrinter implements Visitor<Menu>{

    @Override
    public void visit(Menu visitee) {
        
        System.out.printf("%30s | %10s\n", visitee.period(),String.valueOf(visitee.status()));
    }

    @Override
    public void beforeVisiting(Menu visitee) {
        
    }

    @Override
    public void afterVisiting(Menu visitee) {
        
    }
    
}
