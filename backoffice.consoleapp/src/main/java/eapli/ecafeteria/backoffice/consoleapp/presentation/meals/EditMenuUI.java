/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.backoffice.consoleapp.presentation.meals;

import eapli.ecafeteria.application.EditMenuController;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.domain.meals.MealType;
import eapli.ecafeteria.domain.meals.Menu;
import eapli.framework.application.Controller;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.presentation.console.SelectWidget;
import eapli.util.Console;
import java.util.*;

/**
 *
 * @author soliveira
 */
public class EditMenuUI extends AbstractUI {

    private EditMenuController theController;

    public EditMenuUI() {
        theController = new EditMenuController();
    }

    protected Controller controller() {
        return this.theController == null ? new EditMenuController() : this.theController;
    }

    @Override
    protected boolean doShow() {
        System.out.println("Select a Menu to edit:");
        
        this.theController = new EditMenuController();
        
        Iterable<eapli.ecafeteria.domain.meals.Menu> menus = this.theController.allMenus();
        SelectWidget<Menu> menuSelector = new SelectWidget<>(menus, new MenuPrinter());

        System.out.printf("%33s | %10s\n", "Period", "Status");
        menuSelector.show();

        this.theController.setMenuToEdit(menuSelector.selectedElement());

        ArrayList<String> options = new ArrayList<>();
        options.add(String.format("Change period (current period: %s", menuSelector.selectedElement().period()));
        options.add("Edit meal");
        options.add("Delete meal");

        final SelectWidget<String> optionSelector = new SelectWidget<>(options, new OptionPrinter());
        optionSelector.show();

        switch (optionSelector.selectedOption()) {
            case 1:
                changePeriod();
                break;
            case 2:
                ediMeal();
                break;
            case 3:
                deleteMeal();
                break;
            default:
                break;
        }

        return false;
    }

    @Override
    public String headline() {
        return "Edit Menu";
    }

    private void changePeriod() {
        final String weekNumber = Console.readLine("Select a new week number:");
        this.theController.defineNewPeriod(Integer.parseInt(weekNumber));
    }

    private void deleteMeal() {
        Meal selectedMeal = listMeals();

        if (selectedMeal != null) {
            this.theController.deleteMeal(selectedMeal);
        }
    }
    
    private void ediMeal(){
        Meal selectedMeal = listMeals();
        
        ArrayList<String> options = new ArrayList<>();
        options.add("Edit number of dishes");
        options.add("Edit meal type");
        
        final SelectWidget<String> optionSelector = new SelectWidget<>(options, new OptionPrinter());
        optionSelector.show();

        switch (optionSelector.selectedOption()) {
            case 1:
                editNumberOfDishes(selectedMeal);
                break;
            case 2:
                editMealType(selectedMeal);
                break;
            default:
                break;
        }
        
    }
    
    private void editNumberOfDishes(Meal meal){
        int newNumber = Console.readInteger("Number of dishes:");
        this.theController.editNumberOfDishes(meal, newNumber);
    }
    
    private void editMealType(Meal meal){
        System.out.println("Select a meal type:");
        MealTypeAdapter mealTypeAdapter = new MealTypeAdapter();

        final SelectWidget<MealType> mealTypeSelector = new SelectWidget<>(mealTypeAdapter, new MealTypePrinter());
        mealTypeSelector.show();

        this.theController.editMealType(mealTypeSelector.selectedElement(), meal);
        
    }

    private Meal listMeals() {

        Iterable<Meal> meals = this.theController.allMealsForSelectedMenu();

        System.out.printf("%13s | %20s | %20s | %10s | %7s \n", "Date", "Type", "Dish", "Meal", "#Dishes");

        final SelectWidget<Meal> mealSelector = new SelectWidget<>(meals, new MealPrinter());
        mealSelector.show();

        return mealSelector.selectedElement();
    }
}
