/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.backoffice.consoleapp.presentation.bookings;

import eapli.ecafeteria.AppSettings;
import eapli.ecafeteria.application.ListExistingBookingsController;
import eapli.ecafeteria.domain.mealbooking.Booking;
import eapli.ecafeteria.domain.mealbooking.MecanographicNumber;
import eapli.ecafeteria.persistence.CafeteriaUserRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.framework.application.Controller;
import eapli.framework.presentation.console.AbstractUI;
import eapli.util.Console;

/**
 *
 * @author Pedro Costa
 */
public class ListExistingBookingsUI extends AbstractUI
{
    private final ListExistingBookingsController theController = 
                new ListExistingBookingsController();

    protected Controller controller() {
        return this.theController;
    }

    @Override
    protected boolean doShow() {
        StringBuilder options = new StringBuilder();
        options.append("1. List by date (yyyy-MM-dd ex: 2016-05-25)\n");
        options.append("2. List by Dish type\n");
        options.append("3. List by Dish name\n");
        options.append("4. List by Meal type\n");
        options.append("5. List all\n");
        options.append("0. Exit\n");

        System.out.println(options.toString());

        final int option = Console.readOption(1, 5, 0);

        String criteriaName = "";

        switch (option) {
            case 0:
                return false;
            case 1:
                criteriaName = "bookingDate";
                break;
            case 2:
                criteriaName = "dishType";
                break;
            case 3:
                criteriaName = "dishName";
                break;
            case 4:
                criteriaName = "mealType";
                break;
            case 5:
                criteriaName = "all";
                break;
        }


        final Iterable<Booking> bookings;

        if (option != 5) {
            String criteriaValue = Console.readLine("Introduza o valor pelo qual serão filtradas as reservas.");
            bookings = this.theController.listExistingBookings(criteriaName, criteriaValue);
        } else {
            bookings = this.theController.listAllBookings();
        }

        if (!bookings.iterator().hasNext()) {
            System.out.println("There is no bookings");
        } else {
            for (final Booking booking : bookings) {
                System.out.printf(booking.toString());
            }
        }
        return false;
    }

    @Override
    public String headline()
    {
        return "List Existing Bookings";
    }
    
}
