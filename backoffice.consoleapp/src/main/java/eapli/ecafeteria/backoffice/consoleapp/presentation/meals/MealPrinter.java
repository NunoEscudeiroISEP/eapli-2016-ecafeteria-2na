/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.backoffice.consoleapp.presentation.meals;

import eapli.ecafeteria.domain.meals.Meal;
import eapli.framework.visitor.Visitor;

/**
 *
 * @author soliveira
 */
public class MealPrinter implements Visitor<Meal> {

    @Override
    public void visit(Meal visitee) {
        System.out.printf("%10s | %20s | %20s | %10s | %5d \n",visitee.formatedMealDate(), visitee.dishType().description(),visitee.dishName(),visitee.mealType(), visitee.numberOfDishes());
    }

    @Override
    public void beforeVisiting(Meal visitee) {

    }

    @Override
    public void afterVisiting(Meal visitee) {

    }

}
