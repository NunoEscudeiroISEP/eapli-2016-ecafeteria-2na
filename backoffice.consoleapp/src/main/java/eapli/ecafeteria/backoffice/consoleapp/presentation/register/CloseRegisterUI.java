/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.backoffice.consoleapp.presentation.register;

import eapli.ecafeteria.application.CloseRegisterController;
import eapli.framework.application.Controller;
import eapli.framework.presentation.console.AbstractUI;
import eapli.util.Console;

/**
 *
 * @author guest
 */
public class CloseRegisterUI extends AbstractUI {
    private final CloseRegisterController theController = new CloseRegisterController();

    protected Controller controller() {
        return this.theController;
    }

    @Override
    protected boolean doShow() {
        
        String readLine = Console.readLine("Confirm register close? (Yes/no)");
        
        if(readLine.equalsIgnoreCase("") || readLine.equalsIgnoreCase("yes"))
        {
            if(this.theController.isClose())
            {
                System.out.println("Register already closed");
                return true;
            }
            if(this.theController.closeRegister())
            {
                System.out.println("Register is now CLOSED.");
                this.theController.saveRegister();
                return true;
            }
            else
            {
                System.out.println("ERROR while closing Register.");
            }
        }
        return false;
    }

    @Override
    public String headline() {
        return "Close Register";
    }   
}
