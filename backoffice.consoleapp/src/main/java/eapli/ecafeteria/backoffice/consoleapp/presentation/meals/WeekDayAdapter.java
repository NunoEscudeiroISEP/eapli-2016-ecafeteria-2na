/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.backoffice.consoleapp.presentation.meals;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

/**
 *
 * @author soliveira
 */
public class WeekDayAdapter implements Collection<WeekDay>{

    @Override
    public int size() {
        return WeekDay.values().length;
    }

    @Override
    public boolean isEmpty() {
        return WeekDay.values().length == 0;
    }

    @Override
    public boolean contains(Object o) {
        return false;
    }

    @Override
    public Iterator<WeekDay> iterator() {
        ArrayList<WeekDay> i = new ArrayList<>();
        for (WeekDay w : WeekDay.values()) {
            i.add(w);
        }
        return i.iterator();
    }

    @Override
    public Object[] toArray() {
        return WeekDay.values();
    }

    @Override
    public <T> T[] toArray(T[] a) {
        return null;
    }

    @Override
    public boolean add(WeekDay e) {
        return false;
    }

    @Override
    public boolean remove(Object o) {
        return false;
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        return false;
    }

    @Override
    public boolean addAll(Collection<? extends WeekDay> c) {
        return false;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        return false;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        return false;
    }

    @Override
    public void clear() {
        
    }
    
}
