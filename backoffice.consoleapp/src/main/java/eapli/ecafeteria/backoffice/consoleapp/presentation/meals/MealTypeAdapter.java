/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.backoffice.consoleapp.presentation.meals;

import eapli.ecafeteria.domain.meals.MealType;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

/**
 *
 * @author soliveira
 */
public class MealTypeAdapter implements Collection<MealType> {

    @Override
    public int size() {
        return MealType.values().length;
    }

    @Override
    public boolean isEmpty() {
        return MealType.values().length == 0;
    }

    @Override
    public boolean contains(Object o) {
        return false; // TODO
    }

    @Override
    public Iterator<MealType> iterator() {
        ArrayList<MealType> i = new ArrayList<>();
        for (MealType m : MealType.values()) {
            i.add(m);
        }
        return i.iterator();
    }

    @Override
    public Object[] toArray() {
        return MealType.values();
    }

    @Override
    public <T> T[] toArray(T[] a) {
        return null; // TODO
    }

    @Override
    public boolean add(MealType e) {
        return false; //não se adiciona nada
    }

    @Override
    public boolean remove(Object o) {
        return false; //não se remove nada
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        return false;
    }

    @Override
    public boolean addAll(Collection<? extends MealType> c) {
        return false;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        return false;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        return false;
    }

    @Override
    public void clear() {

    }

}
