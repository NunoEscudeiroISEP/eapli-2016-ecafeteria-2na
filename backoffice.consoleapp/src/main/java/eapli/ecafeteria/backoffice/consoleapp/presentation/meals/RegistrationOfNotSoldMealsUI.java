/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.backoffice.consoleapp.presentation.meals;

import eapli.ecafeteria.application.RegistrationOfNotSoldMealsController;
import eapli.ecafeteria.domain.meals.DishType;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.framework.application.Controller;
import eapli.framework.presentation.console.AbstractUI;
import eapli.util.Console;

import java.time.Clock;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 *
 * @author André Oliveira
 */
public class RegistrationOfNotSoldMealsUI extends AbstractUI {

    private final RegistrationOfNotSoldMealsController theController
            = new RegistrationOfNotSoldMealsController();

    protected Controller controller() {
        return this.theController;
    }

    @Override
    protected boolean doShow() {

        if (theController.activeRegister()) {
            System.out.println("Caixa ainda se encontra aberta!");
            return false;
        }

        if (!theController.findMenuByDate()) {
            System.out.print("Não existem menus para a data atual");
            return false;
        }

        Iterable<DishType> dishTypeList = theController.listAllMealsTypes();
        StringBuilder options = new StringBuilder();

        Iterator<DishType> iterator = dishTypeList.iterator();
        List<DishType> dishTypeListTemp = new ArrayList<>();

        int index = 1;

        while (iterator.hasNext()) {

            options.append(index);
            options.append(".");

            DishType dishType = iterator.next();
            options.append(dishType.description());
            dishTypeListTemp.add(dishType);

            options.append("\n");

            index++;
        }

        System.out.println(options.toString());

        final int option = Console.readOption(1, index - 1, 0);

        System.out.println("\n");

        List<Meal> listAtualMeals = this.theController.mealsOfType(dishTypeListTemp.get(option - 1));

        for (Meal meal : listAtualMeals) {

            Boolean value = Console.readBoolean("Calcular número de pratos não vendidos mas "
                    + "confecionados para " + meal.dishName() + "(Y|N)?");
            
            if (value) {
                
                if (theController.registrationOfNotSoldMeals(meal) == null) {
                    return false;
                } else {
                    System.out.println("Registo de refeições não vendidas para: " + meal.dishName());
                }
            }
        }

        return true;

    }

    @Override
    public String headline() {
        return "Registration of Not Sold Meals";
    }

}
